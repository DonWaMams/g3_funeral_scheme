/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dariunhouse.g3.funeralscheme.form.premiums;

import com.dariunhouse.g3.connection.ConnectServer;
import dbTables.BankDetails;
import dbTables.Family;
import dbTables.Policy;
import java.awt.CardLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author BT Rubber
 */
public class Premiums extends javax.swing.JPanel {

    /**
     * Creates new form PremiumNPay
     */
    public Premiums() {
        initComponents();
        
        getInfo();
//        cmbShowTable.setSelectedIndex(1);
        
        lstSelectionMdl =  tblPremiums.getSelectionModel();
        lstSelectionMdl.addListSelectionListener(new SharedListSelectionHandler());
        tblPremiums.setSelectionModel(lstSelectionMdl);
    }
    
    ConnectServer connect = new ConnectServer();
    double coverAmnt = 0;
    double premium = 0;
    double val = 1000;
    double resAmnt = 0;

    final TableRowSorter<TableModel> sorter1 = new TableRowSorter<TableModel>();
    ListSelectionModel lstSelectionMdl;
    String clientAge;
    String entityId;
    class SharedListSelectionHandler implements ListSelectionListener {
    public void valueChanged(ListSelectionEvent e) {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        try{
            lstSelectionMdl.setSelectionMode(
                            ListSelectionModel.SINGLE_INTERVAL_SELECTION);
            if(tabPremiums.getSelectedIndex() == 0 && tblPremiums.getSelectedRows().length > 0) {
                txtPolicyNo.setText(tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 3).toString());
                txtPolicyNo2.setText(tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 3).toString());
                try{
                        clientAge = connect.premiumBeanLocal().getClientAge(txtPolicyNo.getText());
                        entityId = connect.premiumBeanLocal().getSingleNationalID(txtPolicyNo.getText());
                        
                        cmbCoverType.setSelectedIndex(0);
                        txtAgeOfIndi.setText(null);
                        cmbBasicCover.setSelectedIndex(0);
                        txtBasicPremium.setText(null);
                        
                        txtPolcName.setText(null);
                        txtPolcSurname.setText(null);
                        txtPolcAge.setText(null);
                        txtPolcGender.setText(null);

                        txtPolcBasCover.setText(null);
                        txtPolcBasPremium.setText(null);
                        txtPolcTtlPremium.setText(null);
                        
                        txtPolcStatus.setText(null);
                        txtPolcExtStatus1.setText(null);
                        cmbStatus.setSelectedIndex(0);
                        txtPolcExtTotalPremium.setText(null);
                        txtTotalPremium.setText(null);
                        
                        cmbCoverType.setSelectedItem(connect.premiumBeanLocal().getSingleCoverType(txtPolicyNo.getText()));
                        txtAgeOfIndi.setText(connect.premiumBeanLocal().getSingleAgeRange(txtPolicyNo.getText()));
                        cmbBasicCover.setSelectedItem(connect.premiumBeanLocal().getSingleCoverAmount(txtPolicyNo.getText()));
                        txtBasicPremium.setText(connect.premiumBeanLocal().getSingleBasicPremium(txtPolicyNo.getText()));
                        
                        txtPolcName.setText(connect.premiumBeanLocal().getSingleName(txtPolicyNo.getText()));
                        txtPolcSurname.setText(connect.premiumBeanLocal().getSingleSurname(txtPolicyNo.getText()));
                        txtPolcAge.setText(connect.premiumBeanLocal().getSingleAge(txtPolicyNo.getText()));
                        txtPolcGender.setText(connect.premiumBeanLocal().getSingleGender(txtPolicyNo.getText()));

                        txtPolcBasCover.setText(connect.premiumBeanLocal().getSingleCoverAmount(txtPolicyNo.getText()));
                        txtPolcBasPremium.setText(connect.premiumBeanLocal().getSingleBasicPremium(txtPolicyNo.getText()));
                        try{
                            txtPolcStatus.setText(cmbStatus.getItemAt(Integer.parseInt(connect.premiumBeanLocal().getSingleStatus(txtPolicyNo.getText()))).toString());
                            txtPolcExtStatus1.setText(txtPolcStatus.getText());
                            cmbStatus.setSelectedItem(txtPolcStatus.getText());
                        }
                        catch(Exception ex){
                            ex.printStackTrace();
                        }
                        txtPolcTtlPremium.setText(connect.premiumBeanLocal().getSingleTotalPremium(txtPolicyNo.getText()));
//                        System.out.println(clientAge);
                        txtPolcExtTotalPremium.setText(txtPolcTtlPremium.getText());
                        txtTotalPremium.setText(txtPolcTtlPremium.getText());
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            else if(tabPremiums.getSelectedIndex() == 1){
                if(tblPremiums.getSelectedRows().length > 0) {
                    try{
                        String idNumber = tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 1).toString();
                        
                        clientAge = connect.premiumBeanLocal().getExtFamilyAge(tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 1).toString());
                        
                        txtPolcName1.setText(null);
                        txtPolcSurname1.setText(null);
                        txtPolcAge1.setText(null);
                        txtPolcGender1.setText(null);
                        
                        txtAgeOfExtIndi.setText(null);
//                        txtPolcExtBasicCover.setText(null);
                        cmbBasicCoverP.setSelectedIndex(0);
//                        txtPolcExtBasicPremium.setText(null);
                        txtExtCoverP.setText(null);
                        txtExtPremAmountP.setText(null);
                        
                        txtPolcName1.setText(connect.premiumBeanLocal().getSingleExtName(tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 1).toString()));
                        txtPolcSurname1.setText(connect.premiumBeanLocal().getSingleExtSurname(tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 1).toString()));
                        txtPolcAge1.setText(connect.premiumBeanLocal().getSingleExtAge(tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 1).toString()));
                        txtPolcGender1.setText(connect.premiumBeanLocal().getSingleExtGender(tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 1).toString()));
                        
                        txtAgeOfExtIndi.setText(connect.premiumBeanLocal().getSingleExtAgeRange(idNumber));
                        txtPolcExtBasicCover.setText(connect.premiumBeanLocal().getSingleExtBasicCover(idNumber));
                        cmbBasicCoverP.setSelectedItem(connect.premiumBeanLocal().getSingleExtBasicCover(idNumber));
                        txtPolcExtBasicPremium.setText(connect.premiumBeanLocal().getSingleExtPremPlus(idNumber));
                        txtExtCoverP.setText(connect.premiumBeanLocal().getSingleExtCoverPlus(idNumber));
                        txtExtPremAmountP.setText(connect.premiumBeanLocal().getSingleExtPremPlus(idNumber));
                        
                        if(txtExtCoverP.getText().isEmpty()){
                        System.out.println(clientAge);
                            
                        if(!arrAge.isEmpty()) {
                            outerloop:
                            for(int x = 1; x < arrAge.size(); x++){
                                String age1 = arrAge.get(x).toString();
                                age1 = age1.substring(0, age1.indexOf("-")-1);
                                String age2 = arrAge.get(x).toString();
                                age2 = age2.substring(age2.indexOf("-") + 2, age2.indexOf("-") + 4);

                                if(Integer.parseInt(age1) <= Integer.parseInt(clientAge) && Integer.parseInt(age2) >= Integer.parseInt(clientAge)){
                                    txtAgeOfExtIndi.setText(arrAge.get(x).toString());
                                    break outerloop;
                                }
                                else {
                                    if(x == arrAge.size()){
                                        JOptionPane.showMessageDialog(null, "Sorry, your age is not covered by the chosen plan.");
                                    }
                                }
                            }
                        }
                            
                        String amount = "";

                        if(!txtPolcExtBasicCover.getText().isEmpty()) {
                            coverAmnt = Double.parseDouble(txtPolcExtBasicCover.getText().substring(1, 3) + txtPolcExtBasicCover.getText().substring(txtPolcExtBasicCover.getText().lastIndexOf(" ")+1));
                        }
                        if(!txtPolcTtlPremium.getText().isEmpty()) {
                            premium = Double.parseDouble(txtPolcTtlPremium.getText().substring(1));
                        }
                        System.out.println("1 c:" + coverAmnt + " p:" + premium);
                        switch(arrAge.indexOf(txtAgeOfExtIndi.getText())){
                            case 1: amount = "R2.00";
                                break;
                            case 2: amount = "R3.00";
                                break;
                            case 3: amount = "R4.00";
                                break;
                            case 4: amount = "R4.50";
                                break;
                            case 5: amount = "R5.00";
                                break;
                            case 6: amount = "R6.00";
                                break;
                            case 7: amount = "R8.00";
                                break;
                            case 8: amount = "R10.00";
                                break;
                            case 9: amount = "R15.00";
                                break;
                            case 10: amount = "R25.00";
                                break;
                        }

                        txtExtCoverP.setText(amount);
                        
                //        }
                        }
                    }
                    catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
                else {
//                    txtPolicyNo.setText(tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 3).toString());
//                    txtPolicyNo2.setText(tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 3).toString());
                    
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    }

    private void getInfo(){
        DefaultTableModel dtm = new DefaultTableModel();
//        dtm.addColumn("Client ID", connect.clientBeanLocal().getClientID().toArray());
        dtm.addColumn("ID Number", connect.clientBeanLocal().getClientIDNo().toArray());
        dtm.addColumn("Name", connect.clientBeanLocal().getClientName().toArray());
        dtm.addColumn("Surname", connect.clientBeanLocal().getClientSurname().toArray());
        dtm.addColumn("Policy No", connect.clientBeanLocal().getPolicyNo().toArray());
        dtm.addColumn("Cover Amount", connect.clientBeanLocal().getClientCoverAmount().toArray());
        
        tblPremiums.setModel(dtm);
    }
    
    private void getInfo2(String polcId) {
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.addColumn("Policy", connect.famBeanLocal().getPolicyNo(polcId).toArray());
        dtm.addColumn("National ID", connect.famBeanLocal().getNationalID(polcId).toArray());
        dtm.addColumn("Name", connect.famBeanLocal().getName(polcId).toArray());
        dtm.addColumn("Surname", connect.famBeanLocal().getSurname(polcId).toArray());
        dtm.addColumn("Relationship", connect.famBeanLocal().getRelationship(polcId).toArray());
        
        tblPremiums.setModel(dtm);
    }
    
    ArrayList arrAge = new ArrayList();
    
    private void loadCMBValuesAge(String ent){
        ArrayList arr = new ArrayList();
        if(ent.equals("client")){
            arr.add("Select");
            arr.add("18 - 64 years");
            arr.add("65 - 74 years");
            arr.add("75 - 84 years");
        }
        else if(ent.equals("child")){
            arr.add("Select");
            arr.add("18 - 64 years");
            arr.add("65 - 74 years");
        }
        else if(ent.equals("fam")){
            arr.add("Select");
            arr.add("0 - 13 years");
            arr.add("14 - 25 years");
            arr.add("26 - 30 years");
            arr.add("31 - 40 years");
            arr.add("41 - 50 years");
            arr.add("51 - 60 years");
            arr.add("61 - 65 years");
            arr.add("66 - 70 years");
            arr.add("71 - 75 years");
            arr.add("76 - 85 years");
        }
        
//        cmbAgeClient.removeAllItems();
//        for(int x = 0; x < arr.size(); x++){
//            cmbAgeClient.addItem(arr.get(x));
//        }
        
        arrAge = arr;
    }
    
    private void loadCMBValuesAmount(int val){
        ArrayList arr = new ArrayList();
        if(val == 1){
            arr.add("Select");
            arr.add("R5 000");
            arr.add("R7 500");
            arr.add("R10 000");
            arr.add("R15 000");
            arr.add("R18 000");
            
        }
//        else if(val == 2){
//            arr.add("Select");
//            arr.add("R 1 000");
//        }
        
        cmbBasicCover.removeAllItems();
        for(int x = 0; x < arr.size(); x++){
            cmbBasicCover.addItem(arr.get(x));
        }
    }
    
    private void setPremiumAmount() {
        String amount = "";
        
        if(cmbCoverType.getSelectedIndex() == 1){
            //client
            if(txtAgeOfIndi.getText().equals(arrAge.get(1))){
                System.out.println("Hello");
                switch(cmbBasicCover.getSelectedIndex()){
                    case 1: amount = "R25.00";
                        break;
                    case 2: amount = "R35.00";
                        break;
                    case 3: amount = "R45.00";
                        break;
                    case 4: amount = "R60.00";
                        break;
                    case 5: amount = "R70.00";
                        break;
                }
            }
            else if(txtAgeOfIndi.getText().equals(arrAge.get(2))){
                switch(cmbBasicCover.getSelectedIndex()){
                    case 1: amount = "R40.00";
                        break;
                    case 2: amount = "R55.00";
                        break;
                    case 3: amount = "R65.00";
                        break;
                    case 4: amount = "R90.00";
                        break;
                    case 5: amount = "R110.00";
                        break;
                }
            }
            else if(txtAgeOfIndi.getText().equals(arrAge.get(3))){
                switch(cmbBasicCover.getSelectedIndex()){
                    case 1: amount = "R65.00";
                        break;
                    case 2: amount = "R90.00";
                        break;
                    case 3: amount = "R120.00";
                        break;
                    case 4: amount = "R170.00";
                        break;
                    case 5: amount = "R200.00";
                        break;
                }
            }
        }
        else if(cmbCoverType.getSelectedIndex() == 2){
            //client with children
            if(txtAgeOfIndi.getText().equals(arrAge.get(1))){
                switch(cmbBasicCover.getSelectedIndex()){
                    case 1: amount = "R30.00";
                        break;
                    case 2: amount = "R45.00";
                        break;
                    case 3: amount = "R55.00";
                        break;
                    case 4: amount = "R75.00";
                        break;
                    case 5: amount = "R90.00";
                        break;
                }
            }
            else if(txtAgeOfIndi.getText().equals(arrAge.get(2))){
                switch(cmbBasicCover.getSelectedIndex()){
                    case 1: amount = "R50.00";
                        break;
                    case 2: amount = "R65.00";
                        break;
                    case 3: amount = "R75.00";
                        break;
                    case 4: amount = "R100.00";
                        break;
                    case 5: amount = "R120.00";
                        break;
                }
            }
        }
        else if(cmbCoverType.getSelectedIndex() == 3){
            //family with children
            if(txtAgeOfIndi.getText().equals(arrAge.get(1))) {
                switch(cmbBasicCover.getSelectedIndex()) {
                    case 1: amount = "R40.00";
                        break;
                    case 2: amount = "R55.00";
                        break;
                    case 3: amount = "R75.00";
                        break;
                    case 4: amount = "R100.00";
                        break;
                    case 5: amount = "R120.00";
                        break;
                }
            }
            else if(txtAgeOfIndi.getText().equals(arrAge.get(2))){
                switch(cmbBasicCover.getSelectedIndex()){
                    case 1: amount = "R70.00";
                        break;
                    case 2: amount = "R100.00";
                        break;
                    case 3: amount = "R130.00";
                        break;
                    case 4: amount = "R180.00";
                        break;
                    case 5: amount = "R210.00";
                        break;
                }
            }
        }
        
        System.out.println(amount);
        System.out.println(cmbBasicCover.getSelectedIndex());
        System.out.println(arrAge.get(1));
        System.out.println(txtAgeOfIndi.getText());
        txtBasicPremium.setText(amount);
        txtPolcExtBasicCover.setText(amount);
        
        if(txtPolcTtlPremium.getText().isEmpty()) {
            txtPolcTtlPremium.setText(txtBasicPremium.getText());
        }
    }
    
    private void updPolicy() {
        Policy polc = new Policy();
        
        polc.setChosenPlan(cmbCoverType.getSelectedItem().toString());
        polc.setAgeRange(txtAgeOfIndi.getText());
        polc.setCoverAmount(cmbBasicCover.getSelectedItem().toString());
        polc.setBasicPremium(txtBasicPremium.getText());
        polc.setPolicyStatusId(cmbStatus.getSelectedIndex() + "");
        polc.setTotalPremium(txtPolcTtlPremium.getText());
        
        txtPolcBasCover.setText(cmbBasicCover.getSelectedItem().toString());
        txtPolcBasPremium.setText(txtBasicPremium.getText());
        txtPolcStatus.setText(cmbStatus.getSelectedItem().toString());
        txtPolcExtStatus1.setText(cmbStatus.getSelectedItem().toString());
        txtPolcTtlPremium.setText(txtBasicPremium.getText());
        
//        if(!txtPolcExtTotalPremium.getText().isEmpty()) {
//            int tltPrem1 = Integer.parseInt(txtPolcTtlPremium.getText().substring(1));
//            int tltPrem2 = Integer.parseInt(txtPolcExtTotalPremium.getText().substring(1));
//            
//            int tlt = tltPrem1 + tltPrem2;
//            txtPolcTtlPremium.setText(tlt + "");
//        }
        
        if(connect.premiumBeanLocal().updBasPremium(polc, txtPolicyNo.getText()).equals("success")) {
            JOptionPane.showMessageDialog(null, "Successfully update your policy information.");
        }
    }
    
    private void updPolicyExt() {
        Policy polc = new Policy();
        Family fam = new Family();
        
        fam.setAgeRange(txtAgeOfExtIndi.getText());
        fam.setBasicCover(cmbBasicCoverP.getSelectedItem().toString());
        fam.setCoverPlus(txtExtCoverP.getText());
        fam.setPremiumPlus(txtExtPremAmountP.getText());
        
        polc.setTotalPremium(txtPolcExtTotalPremium.getText());
        
        polc.setPolicyNo(txtPolicyNo2.getText());
        fam.setPolicyNo(txtPolicyNo2.getText());
        
//        if(!txtPolcExtTotalPremium.getText().isEmpty()) {
//            double tltPrem1 = Integer.parseInt(txtPolcTtlPremium.getText().substring(1));
//            double tltPrem2 = Integer.parseInt(txtPolcExtTotalPremium.getText().substring(1));
//            
//            double tlt = tltPrem1 + tltPrem2;
//            txtPolcTtlPremium.setText(tlt + "");
//        }
        
        if(connect.premiumBeanLocal().updPolicyExtPremium(polc, fam, tblPremiums.getValueAt(tblPremiums.getSelectedRow(), 1).toString()).equals("success")) {
            JOptionPane.showMessageDialog(null, "Successfully update your policy information.");
        }
    }
    
    private void updPaymentDetails() {
        Policy polc = new Policy();
        BankDetails bank = new BankDetails();
        
        String chkd = "";
        if(rbtnBnkDebitOrder.isSelected()) {
            chkd = rbtnBnkDebitOrder.getText();
        }
        else if(rbtnCash.isSelected()){
            chkd = rbtnCash.getText();
        }
        
        polc.setPaymentMethod(chkd);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        //polc.setPaymentStart(sdf.format(dateOfPayment.getDate()));
        polc.setDayOfPayment(txtDayOfPayment.getText());
        polc.setPolicyNo(txtPolicyNo.getText());
        
        bank.setNameOfBank(txtNameOfBank.getText());
        bank.setBranch(txtBranch.getText());
        bank.setBranchCode(txtBranchCode.getText());
        bank.setAccountNo(txtAccountNo.getText());
        bank.setTypeOfAccount(cmbAccountType.getSelectedItem().toString());
        bank.setEntityId(entityId);
        
        if(connect.premiumBeanLocal().updPolicyPayments(polc, bank).equals("success")) {
            JOptionPane.showMessageDialog(null, "Payment details have been successfully saved.");
        }
        else {
            JOptionPane.showMessageDialog(null, "There was a problem in saving your details.\nCheck if the information you entered is correct.");
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        planGroup = new javax.swing.ButtonGroup();
        premiumGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        tabPremiums = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        btnSubmitBasPrem = new javax.swing.JButton();
        pnlPremium = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jLabel38 = new javax.swing.JLabel();
        txtBasicPremium = new javax.swing.JTextField();
        cmbCoverType = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        lblClient1 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        cmbBasicCover = new javax.swing.JComboBox();
        txtAgeOfIndi = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cmbStatus = new javax.swing.JComboBox();
        jLabel42 = new javax.swing.JLabel();
        txtPolicyNo = new javax.swing.JTextField();
        btnConfirm = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        pnlPolicyInfo = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtPolcTtlPremium = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtPolcBasCover = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        txtPolcBasPremium = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        txtPolcStatus = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        txtPolcName = new javax.swing.JTextField();
        jLabel76 = new javax.swing.JLabel();
        txtPolcSurname = new javax.swing.JTextField();
        jLabel79 = new javax.swing.JLabel();
        txtPolcGender = new javax.swing.JTextField();
        txtPolcAge = new javax.swing.JTextField();
        jLabel78 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        btnClear = new javax.swing.JButton();
        btnPreviousBas = new javax.swing.JButton();
        btnNextBas = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        pnlFinancials1 = new javax.swing.JPanel();
        txtExtPremAmountP = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        lblClient = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        txtAgeOfExtIndi = new javax.swing.JTextField();
        txtExtCoverP = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        cmbBasicCoverP = new javax.swing.JComboBox();
        btnSubmitExtPrem = new javax.swing.JButton();
        jLabel45 = new javax.swing.JLabel();
        txtPolicyNo2 = new javax.swing.JTextField();
        btnExtConfirm = new javax.swing.JButton();
        jPanel22 = new javax.swing.JPanel();
        pnlFinancials3 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        txtPolcExtTotalPremium = new javax.swing.JTextField();
        txtPolcExtBasicPremium = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtPolcExtBasicCover = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        txtPolcExtStatus1 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        jLabel81 = new javax.swing.JLabel();
        txtPolcAge1 = new javax.swing.JTextField();
        txtPolcGender1 = new javax.swing.JTextField();
        txtPolcSurname1 = new javax.swing.JTextField();
        jLabel77 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        txtPolcName1 = new javax.swing.JTextField();
        btnClear2 = new javax.swing.JButton();
        btnNextBas1 = new javax.swing.JButton();
        btnPreviousBas1 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        btnSubmitBankDetails = new javax.swing.JButton();
        pnlPremiumPayment = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        rbtnBnkDebitOrder = new javax.swing.JRadioButton();
        rbtnCash = new javax.swing.JRadioButton();
        jLabel13 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtTotalPremium = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtDayOfPayment = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel18 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtNameOfBank = new javax.swing.JTextField();
        txtBranch = new javax.swing.JTextField();
        txtBranchCode = new javax.swing.JTextField();
        txtAccountNo = new javax.swing.JTextField();
        cmbAccountType = new javax.swing.JComboBox();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        chkAgree = new javax.swing.JCheckBox();
        jLabel19 = new javax.swing.JLabel();
        btnPreviousPay = new javax.swing.JButton();
        btnNextPay = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        txtSearch = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPremiums = new javax.swing.JTable();
        cmbShowTable = new javax.swing.JComboBox();
        btnPrint = new javax.swing.JButton();
        cmbPrint = new javax.swing.JComboBox();
        btnExport = new javax.swing.JButton();
        cmbExport = new javax.swing.JComboBox();
        jLabel32 = new javax.swing.JLabel();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        tabPremiums.setFocusable(false);
        tabPremiums.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tabPremiums.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tabPremiumsStateChanged(evt);
            }
        });
        tabPremiums.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabPremiumsMouseClicked(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        btnSubmitBasPrem.setText("Submit");
        btnSubmitBasPrem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitBasPremActionPerformed(evt);
            }
        });

        pnlPremium.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Premium Calculations", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        pnlPremium.setLayout(new java.awt.CardLayout());

        jLabel38.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel38.setText("Basic Premium");

        txtBasicPremium.setEditable(false);
        txtBasicPremium.setColumns(2);

        cmbCoverType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Single client funeral plan", "Single client with children funeral plan", "Family with children funeral plan" }));
        cmbCoverType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCoverTypeItemStateChanged(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Type Of Cover");

        jLabel36.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel36.setText("Age Of");

        lblClient1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblClient1.setText("indiv.");

        jLabel43.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel43.setText("Basic Cover");

        cmbBasicCover.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "R 5 000", "R 7 500", "R 10 000", "R 15 000", "R 18 000" }));
        cmbBasicCover.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbBasicCoverItemStateChanged(evt);
            }
        });

        txtAgeOfIndi.setEditable(false);
        txtAgeOfIndi.setColumns(2);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Status");

        cmbStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "In-Force", "Suspend", "Cancelled", "Deceased", "Over-Age" }));
        cmbStatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbStatusItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblClient1))
                    .addComponent(jLabel43)
                    .addComponent(jLabel38))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbBasicCover, 0, 121, Short.MAX_VALUE)
                    .addComponent(txtBasicPremium)
                    .addComponent(cmbCoverType, javax.swing.GroupLayout.Alignment.TRAILING, 0, 1, Short.MAX_VALUE)
                    .addComponent(txtAgeOfIndi))
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbStatus, 0, 125, Short.MAX_VALUE)
                .addGap(32, 32, 32))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbCoverType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(cmbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(lblClient1)
                    .addComponent(txtAgeOfIndi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbBasicCover, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBasicPremium, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel38))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlPremium.add(jPanel17, "card4");

        jLabel42.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel42.setText("Policy No:");

        btnConfirm.setText("Confirm");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlPremium, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnSubmitBasPrem, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel42)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPolicyNo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42)
                    .addComponent(txtPolicyNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirm))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlPremium, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSubmitBasPrem)
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        pnlPolicyInfo.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Policy Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        pnlPolicyInfo.setLayout(new java.awt.CardLayout());

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Total Premium");

        txtPolcTtlPremium.setEditable(false);
        txtPolcTtlPremium.setColumns(2);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Basic Cover");

        txtPolcBasCover.setEditable(false);
        txtPolcBasCover.setColumns(2);

        jLabel39.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel39.setText("Basic Premium");

        txtPolcBasPremium.setEditable(false);
        txtPolcBasPremium.setColumns(2);

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel41.setText("Status");

        txtPolcStatus.setEditable(false);
        txtPolcStatus.setColumns(2);

        jLabel49.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel49.setText("Name");

        jLabel76.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel76.setText("Surname");

        jLabel79.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel79.setText("Gender");

        jLabel78.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel78.setText("Age");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel6)
                    .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel41)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtPolcBasPremium, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                    .addComponent(txtPolcStatus)
                    .addComponent(txtPolcTtlPremium)
                    .addComponent(txtPolcBasCover, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel78)
                            .addComponent(jLabel79))
                        .addGap(17, 17, 17)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPolcGender, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                            .addComponent(txtPolcAge)))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel76)
                            .addComponent(jLabel49))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPolcSurname)
                            .addComponent(txtPolcName))))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtPolcBasCover, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel49)
                    .addComponent(txtPolcName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPolcBasPremium, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39)
                    .addComponent(jLabel76)
                    .addComponent(txtPolcSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPolcStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel41)
                    .addComponent(jLabel78)
                    .addComponent(txtPolcAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtPolcTtlPremium, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel79)
                    .addComponent(txtPolcGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(49, Short.MAX_VALUE))
        );

        pnlPolicyInfo.add(jPanel10, "card2");

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 457, Short.MAX_VALUE)
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 203, Short.MAX_VALUE)
        );

        pnlPolicyInfo.add(jPanel19, "card3");

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnPreviousBas.setText("Previous");
        btnPreviousBas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousBasActionPerformed(evt);
            }
        });

        btnNextBas.setText("Next");
        btnNextBas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextBasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPreviousBas, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNextBas, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(pnlPolicyInfo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(pnlPolicyInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClear)
                    .addComponent(btnPreviousBas)
                    .addComponent(btnNextBas))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        tabPremiums.addTab("Basic Premiums", jPanel3);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        pnlFinancials1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Extended Premium Calculations", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        txtExtPremAmountP.setEditable(false);

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel35.setText("Premium+");

        lblClient.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblClient.setText("indiv.");

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel33.setText("Age Of");

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel34.setText("Cover+");

        txtAgeOfExtIndi.setEditable(false);
        txtAgeOfExtIndi.setColumns(2);

        txtExtCoverP.setEditable(false);
        txtExtCoverP.setColumns(2);

        jLabel37.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel37.setText("Basic Cover");

        cmbBasicCoverP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "R5 000", "R7 500", "R10 000", "R15 000", "R18 000" }));
        cmbBasicCoverP.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbBasicCoverPItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout pnlFinancials1Layout = new javax.swing.GroupLayout(pnlFinancials1);
        pnlFinancials1.setLayout(pnlFinancials1Layout);
        pnlFinancials1Layout.setHorizontalGroup(
            pnlFinancials1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFinancials1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFinancials1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel35)
                    .addGroup(pnlFinancials1Layout.createSequentialGroup()
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblClient))
                    .addComponent(jLabel34)
                    .addComponent(jLabel37))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlFinancials1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtExtCoverP, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbBasicCoverP, javax.swing.GroupLayout.Alignment.LEADING, 0, 126, Short.MAX_VALUE)
                    .addComponent(txtAgeOfExtIndi, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtExtPremAmountP))
                .addGap(238, 238, 238))
        );
        pnlFinancials1Layout.setVerticalGroup(
            pnlFinancials1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFinancials1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFinancials1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(lblClient)
                    .addComponent(txtAgeOfExtIndi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlFinancials1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(cmbBasicCoverP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlFinancials1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(txtExtCoverP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlFinancials1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(txtExtPremAmountP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnSubmitExtPrem.setText("Submit");
        btnSubmitExtPrem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitExtPremActionPerformed(evt);
            }
        });

        jLabel45.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel45.setText("Policy No:");

        btnExtConfirm.setText("Confirm");
        btnExtConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExtConfirmActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlFinancials1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSubmitExtPrem, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel45)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPolicyNo2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnExtConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel45)
                    .addComponent(txtPolicyNo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExtConfirm))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlFinancials1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSubmitExtPrem)
                .addContainerGap())
        );

        jPanel22.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jPanel23.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Policy Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel23.setLayout(new java.awt.CardLayout());

        txtPolcExtTotalPremium.setEditable(false);
        txtPolcExtTotalPremium.setColumns(2);

        txtPolcExtBasicPremium.setEditable(false);
        txtPolcExtBasicPremium.setColumns(2);

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel40.setText("Basic Premium");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Basic Cover");

        txtPolcExtBasicCover.setEditable(false);
        txtPolcExtBasicCover.setColumns(2);

        jLabel44.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel44.setText("Status");

        txtPolcExtStatus1.setEditable(false);
        txtPolcExtStatus1.setColumns(2);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Total Premium");

        jLabel80.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel80.setText("Gender");

        jLabel81.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel81.setText("Age");

        jLabel77.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel77.setText("Surname");

        jLabel50.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel50.setText("Name");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel40)
                    .addComponent(jLabel4)
                    .addComponent(jLabel44))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtPolcExtStatus1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(txtPolcExtBasicPremium, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPolcExtBasicCover)
                    .addComponent(txtPolcExtTotalPremium))
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel81)
                            .addComponent(jLabel80))
                        .addGap(17, 17, 17)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPolcAge1, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                            .addComponent(txtPolcGender1)))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel77)
                            .addComponent(jLabel50))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPolcSurname1)
                            .addComponent(txtPolcName1))))
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtPolcExtBasicCover, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel50)
                    .addComponent(txtPolcName1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPolcExtBasicPremium, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40)
                    .addComponent(jLabel77)
                    .addComponent(txtPolcSurname1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel81)
                        .addComponent(txtPolcAge1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtPolcExtStatus1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel44)))
                .addGap(12, 12, 12)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel80)
                        .addComponent(txtPolcGender1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(txtPolcExtTotalPremium, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(49, Short.MAX_VALUE))
        );

        jPanel23.add(jPanel14, "card2");

        javax.swing.GroupLayout pnlFinancials3Layout = new javax.swing.GroupLayout(pnlFinancials3);
        pnlFinancials3.setLayout(pnlFinancials3Layout);
        pnlFinancials3Layout.setHorizontalGroup(
            pnlFinancials3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlFinancials3Layout.setVerticalGroup(
            pnlFinancials3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        btnClear2.setText("Clear");
        btnClear2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClear2ActionPerformed(evt);
            }
        });

        btnNextBas1.setText("Next");
        btnNextBas1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextBas1ActionPerformed(evt);
            }
        });

        btnPreviousBas1.setText("Previous");
        btnPreviousBas1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousBas1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlFinancials3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnClear2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPreviousBas1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNextBas1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addComponent(pnlFinancials3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClear2)
                    .addComponent(btnPreviousBas1)
                    .addComponent(btnNextBas1))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        tabPremiums.addTab("Extended Premiums", jPanel6);

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        btnSubmitBankDetails.setText("Submit");
        btnSubmitBankDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitBankDetailsActionPerformed(evt);
            }
        });

        pnlPremiumPayment.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0))));
        pnlPremiumPayment.setLayout(new java.awt.CardLayout());

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("I, the premium-payment, will pay the premium monthly by");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setText("Page 1 Of 4");

        premiumGroup.add(rbtnBnkDebitOrder);
        rbtnBnkDebitOrder.setText("Bank Debit Order");

        premiumGroup.add(rbtnCash);
        rbtnCash.setText("Cash");

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setText("I will begin to pay from");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                        .addGap(0, 860, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addGap(6, 6, 6))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbtnBnkDebitOrder)
                            .addComponent(rbtnCash)
                            .addComponent(jLabel13))
                        .addGap(0, 811, Short.MAX_VALUE))))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbtnBnkDebitOrder)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbtnCash)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addContainerGap())
        );

        pnlPremiumPayment.add(jPanel11, "card2");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setText("Page 2 Of 4");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setText("The total monthly premium when the policy begins will be");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("The day of the month on which I prefer payment to be made - for example the 25th of every month");

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel25.setText("State only the day");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel14))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(txtTotalPremium, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16)
                            .addComponent(txtDayOfPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25))
                        .addGap(0, 315, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTotalPremium, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDayOfPayment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                .addComponent(jLabel14)
                .addContainerGap())
        );

        pnlPremiumPayment.add(jPanel13, "card3");

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setText("Page 3 Of 4");

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(2);
        jTextArea1.setLineWrap(true);
        jTextArea1.setText("I authorise Assupol to draw the premiums from my bank account. If the premium changes for any reason in terms of the policy, or by agreement between Assupol and the policyholder, Assupol likewise may draw the premium from my bank account. If payment cannot be done on the preferred day of the month filled in above, it must be done on a day that is as close as possible to that day, determined by Assupol. If the policy ends, the authorisation also ends. I may cancel, amend or replace this authorisation by written notice to Assupol. I accept that Assupol must receive the notice not later than 30 days before the month from which the cancellation, amendment or replacement is to apply.");
        jTextArea1.setWrapStyleWord(true);
        jScrollPane3.setViewportView(jTextArea1);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("Payment by bank debit order");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel17))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel15Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17)
                .addContainerGap())
        );

        pnlPremiumPayment.add(jPanel15, "card4");

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel20.setText("Name of Bank");

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel21.setText("Branch");

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel22.setText("Branch Code");

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setText("Account No.");

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel24.setText("Type Of Account");

        txtNameOfBank.setColumns(2);
        txtNameOfBank.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtBranch.setColumns(2);
        txtBranch.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtBranchCode.setColumns(2);
        txtBranchCode.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtAccountNo.setColumns(2);
        txtAccountNo.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        cmbAccountType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Current", "Savings", "Transmission" }));

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Agreement", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jTextArea2.setEditable(false);
        jTextArea2.setColumns(2);
        jTextArea2.setLineWrap(true);
        jTextArea2.setText("I have read, understand, and agree with the above authorisation regarding payment by bank debit order.");
        jTextArea2.setWrapStyleWord(true);
        jScrollPane4.setViewportView(jTextArea2);

        chkAgree.setText("I Agree");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(chkAgree)
                        .addGap(0, 419, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chkAgree)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel19.setText("Page 4 Of 4");

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21)
                            .addComponent(jLabel22)
                            .addComponent(jLabel23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtBranchCode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNameOfBank, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtBranch, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtAccountNo, javax.swing.GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbAccountType, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel19)))
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(jLabel24)
                    .addComponent(txtNameOfBank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbAccountType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(txtBranch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(txtBranchCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel23)
                            .addComponent(txtAccountNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel19)
                .addContainerGap())
        );

        pnlPremiumPayment.add(jPanel16, "card5");

        btnPreviousPay.setText("Previous");
        btnPreviousPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousPayActionPerformed(evt);
            }
        });

        btnNextPay.setText("Next");
        btnNextPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextPayActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnSubmitBankDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPreviousPay, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNextPay, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(pnlPremiumPayment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(pnlPremiumPayment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSubmitBankDetails)
                    .addComponent(btnPreviousPay)
                    .addComponent(btnNextPay))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        tabPremiums.addTab("Payments & Banking", jPanel8);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPremiums)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPremiums)
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Search:");

        tblPremiums.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblPremiums.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tblPremiums);

        cmbShowTable.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Policy Holder", "Extended Family" }));
        cmbShowTable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbShowTableItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cmbShowTable, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jScrollPane1)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbShowTable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE))
        );

        btnPrint.setText("Print");
        btnPrint.setEnabled(false);

        cmbPrint.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Not Confirmed", "Form", "List" }));

        btnExport.setText("Export");
        btnExport.setEnabled(false);

        cmbExport.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Not Confirmed", "PDF", "Word (DocX)", "Excel (Xlsx)" }));

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel32.setText("Export To:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel32)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbExport, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExport, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cmbPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(cmbExport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExport)
                    .addComponent(cmbPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPreviousPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousPayActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnlPremiumPayment.getLayout();
        cl.previous(pnlPremiumPayment);
    }//GEN-LAST:event_btnPreviousPayActionPerformed

    private void btnNextPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextPayActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnlPremiumPayment.getLayout();
        cl.next(pnlPremiumPayment);
    }//GEN-LAST:event_btnNextPayActionPerformed

    private void cmbCoverTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCoverTypeItemStateChanged
        // TODO add your handling code here:
//        cmbAmount.setEnabled(true);
        txtAgeOfIndi.setText(null);
        if(cmbCoverType.getSelectedIndex() == 1){
            loadCMBValuesAge("client");
            loadCMBValuesAmount(1);
            
        }
        else if(cmbCoverType.getSelectedIndex() == 2 | cmbCoverType.getSelectedIndex() == 3){
            loadCMBValuesAge("child");
            loadCMBValuesAmount(1);
//            lblClient.setText("client");
        }
//        else if(cmbCoverType.getSelectedItem().toString().equals("Extended family")){
//            loadCMBValuesAge("fam");
////            loadCMBValuesAmount(2);
//            
//            
//        }
//        cmbAgeClient.setSelectedIndex(0);
//        cmbAmount.setSelectedIndex(0);
        
        String age3 = "";
        
            if(!arrAge.isEmpty()) {
                for(int x = 1; x < arrAge.size(); x++){
                    String age1 = arrAge.get(x).toString();
                    age1 = age1.substring(0, age1.indexOf("-")-1);
                    String age2 = arrAge.get(x).toString();
                    age2 = age2.substring(age2.indexOf("-") + 2, age2.indexOf("-") + 4);
                    
                    age3 += age1 + "<>" + age2 + "\n";
                    
                    if(Integer.parseInt(age1) <= Integer.parseInt(clientAge) && Integer.parseInt(age2) >= Integer.parseInt(clientAge)){
                        txtAgeOfIndi.setText(arrAge.get(x).toString());
                        break;
                    }
                    else {
                        if(x == arrAge.size()){
                            JOptionPane.showMessageDialog(null, "Sorry, your age is not covered by the chosen plan.");
                        }
                    }
                }
            }
            System.out.println(clientAge + "\n" + age3);
    }//GEN-LAST:event_cmbCoverTypeItemStateChanged

    private void cmbShowTableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbShowTableItemStateChanged
        // TODO add your handling code here:
        if(cmbShowTable.getSelectedIndex() == 1){
            getInfo();
        }
        else if(cmbShowTable.getSelectedIndex() == 2){
            if(tabPremiums.getSelectedIndex() == 0){
                if(!txtPolicyNo.getText().isEmpty()){
                    getInfo2(txtPolicyNo.getText());
                }
                else {
                    JOptionPane.showMessageDialog(null, "Please select a Policy Holder first\nbefore attempting to view subodinates.");
                    cmbShowTable.setSelectedIndex(1);
                }
            }
            else if(tabPremiums.getSelectedIndex() == 1) {
                if(!txtPolicyNo2.getText().isEmpty()){
                    getInfo2(txtPolicyNo2.getText());
                }
                else {
                    JOptionPane.showMessageDialog(null, "Please select a Policy Holder first\nbefore attempting to view subodinates.");
                    cmbShowTable.setSelectedIndex(1);
                }
            }
        }
        
        if(tabPremiums.getSelectedIndex() == 2) {
            cmbShowTable.setSelectedIndex(1);
        }
        
    }//GEN-LAST:event_cmbShowTableItemStateChanged

    private void cmbBasicCoverItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbBasicCoverItemStateChanged
        // TODO add your handling code here:
        setPremiumAmount();
        if(cmbBasicCover.getSelectedIndex() > 0){
            txtPolcBasCover.setText(cmbBasicCover.getSelectedItem().toString());
            txtPolcExtBasicCover.setText(txtPolcBasCover.getText());
            txtPolcBasPremium.setText(txtBasicPremium.getText());
            txtPolcExtBasicPremium.setText(txtPolcBasPremium.getText());
        }
    }//GEN-LAST:event_cmbBasicCoverItemStateChanged

    private void btnSubmitBasPremActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitBasPremActionPerformed
        // TODO add your handling code here:
        updPolicy();
    }//GEN-LAST:event_btnSubmitBasPremActionPerformed

    int selcItem = 0;
    private void tabPremiumsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tabPremiumsStateChanged
        // TODO add your handling code here:
        
        if(tabPremiums.getSelectedIndex() != 0){
            if(!txtPolicyNo.getText().isEmpty()){
                loadCMBValuesAge("fam");
            }
            else {
                tabPremiums.setSelectedIndex(0);
                JOptionPane.showMessageDialog(null, "Please select a Policy Holder before you continue.");
                txtPolicyNo.grabFocus();
            }
        }
        
//        if(tblPremiums.getSelectedRows().length > 0) {
//            selcItem = tblPremiums.getSelectedRow();
//        }
        
        if(tabPremiums.getSelectedIndex() == 0) {
            getInfo();
//            tblPremiums.setRowSelectionInterval(selcItem, selcItem+1);
        }
        else if(tabPremiums.getSelectedIndex() == 1) {
            getInfo2(txtPolicyNo.getText());
//            tblPremiums.setRowSelectionInterval(selcItem, selcItem+1);
        }
        else if(tabPremiums.getSelectedIndex() == 2) {
            getInfo();
//            tblPremiums.setRowSelectionInterval(selcItem, selcItem+1);
        }
    }//GEN-LAST:event_tabPremiumsStateChanged

    private void tabPremiumsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabPremiumsMouseClicked
        // TODO add your handling code here:
//        cmbShowTable.setSelectedIndex(1);
    }//GEN-LAST:event_tabPremiumsMouseClicked

    private void cmbStatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbStatusItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbStatusItemStateChanged

    private void btnSubmitExtPremActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitExtPremActionPerformed
        // TODO add your handling code here:
        updPolicyExt();
    }//GEN-LAST:event_btnSubmitExtPremActionPerformed

    private void btnExtConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExtConfirmActionPerformed
        // TODO add your handling code here:
        clientAge = connect.premiumBeanLocal().getClientAge(txtPolicyNo2.getText());
        txtPolcName1.setText(connect.premiumBeanLocal().getSingleName(txtPolicyNo2.getText()));
        txtPolcSurname1.setText(connect.premiumBeanLocal().getSingleSurname(txtPolicyNo2.getText()));
        txtPolcAge1.setText(connect.premiumBeanLocal().getSingleAge(txtPolicyNo2.getText()));
        txtPolcGender1.setText(connect.premiumBeanLocal().getSingleGender(txtPolicyNo2.getText()));
    }//GEN-LAST:event_btnExtConfirmActionPerformed

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        // TODO add your handling code here:
        clientAge = connect.premiumBeanLocal().getClientAge(txtPolicyNo.getText());
        txtPolcName.setText(connect.premiumBeanLocal().getSingleName(txtPolicyNo.getText()));
        txtPolcSurname.setText(connect.premiumBeanLocal().getSingleSurname(txtPolicyNo.getText()));
        txtPolcAge.setText(connect.premiumBeanLocal().getSingleAge(txtPolicyNo.getText()));
        txtPolcGender.setText(connect.premiumBeanLocal().getSingleGender(txtPolicyNo.getText()));
        
        txtPolcBasCover.setText(connect.premiumBeanLocal().getSingleCoverAmount(txtPolicyNo.getText()));
        txtPolcBasPremium.setText(connect.premiumBeanLocal().getSingleBasicPremium(txtPolicyNo.getText()));
        try{
            txtPolcStatus.setText(cmbStatus.getItemAt(Integer.parseInt(connect.premiumBeanLocal().getSingleStatus(txtPolicyNo.getText()))).toString());
            txtPolcExtStatus1.setText(txtPolcStatus.getText());
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        txtPolcTtlPremium.setText(connect.premiumBeanLocal().getSingleTotalPremium(txtPolicyNo.getText()));
        txtPolcExtTotalPremium.setText(txtPolcTtlPremium.getText());
    }//GEN-LAST:event_btnConfirmActionPerformed

    private void btnPreviousBasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousBasActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnlPolicyInfo.getLayout();
        cl.previous(pnlPolicyInfo);
    }//GEN-LAST:event_btnPreviousBasActionPerformed

    private void btnNextBasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextBasActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnlPolicyInfo.getLayout();
        cl.next(pnlPolicyInfo);
    }//GEN-LAST:event_btnNextBasActionPerformed

    private void btnPreviousBas1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousBas1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPreviousBas1ActionPerformed

    private void btnNextBas1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextBas1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnNextBas1ActionPerformed

    private void btnClear2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClear2ActionPerformed
        // TODO add your handling code here:
        txtPolcName1.setText(null);
        txtPolcSurname1.setText(null);
        txtPolcAge1.setText(null);
        txtPolcGender1.setText(null);

        txtAgeOfExtIndi.setText(null);
        txtPolcExtBasicCover.setText(null);
        cmbBasicCoverP.setSelectedIndex(0);
        txtPolcExtBasicPremium.setText(null);
        txtExtCoverP.setText(null);
        txtExtPremAmountP.setText(null);
    }//GEN-LAST:event_btnClear2ActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        cmbCoverType.setSelectedIndex(0);
        txtAgeOfIndi.setText(null);
        cmbBasicCover.setSelectedIndex(0);
        txtBasicPremium.setText(null);

        txtPolcName.setText(null);
        txtPolcSurname.setText(null);
        txtPolcAge.setText(null);
        txtPolcGender.setText(null);

        txtPolcBasCover.setText(null);
        txtPolcBasPremium.setText(null);
        txtPolcTtlPremium.setText(null);

        txtPolcStatus.setText(null);
        txtPolcExtStatus1.setText(null);
        cmbStatus.setSelectedIndex(0);
        txtPolcExtTotalPremium.setText(null);
        txtTotalPremium.setText(null);

        txtPolcName1.setText(null);
        txtPolcSurname1.setText(null);
        txtPolcAge1.setText(null);
        txtPolcGender1.setText(null);

        txtAgeOfExtIndi.setText(null);
        txtPolcExtBasicCover.setText(null);
        cmbBasicCoverP.setSelectedIndex(0);
        txtPolcExtBasicPremium.setText(null);
        txtExtCoverP.setText(null);
        txtExtPremAmountP.setText(null);
    }//GEN-LAST:event_btnClearActionPerformed

    private void cmbBasicCoverPItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbBasicCoverPItemStateChanged
        // TODO add your handling code here:
        if(cmbBasicCoverP.getSelectedIndex() > 0) {
            try {
                String cvrAmnt = cmbBasicCoverP.getSelectedItem().toString().substring(1);
                coverAmnt = Integer.parseInt(cvrAmnt.replace(" ", ""));
//                System.out.println("2 c:" + coverAmnt);
                resAmnt = coverAmnt / val;
//                System.out.println("3 res: " + resAmnt);
                resAmnt = resAmnt * Double.parseDouble(txtExtCoverP.getText().substring(1));
//                System.out.println("4 res: " + resAmnt);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

            txtPolcExtBasicCover.setText(txtExtCoverP.getText());
            
            txtExtPremAmountP.setText("R" + resAmnt + "0");
            txtPolcExtBasicPremium.setText(txtExtPremAmountP.getText());
            double totalPrem = premium + resAmnt;
//            System.out.println("5 res: " + totalPrem);
            txtPolcTtlPremium.setText("R" + totalPrem + "0");
            txtPolcExtTotalPremium.setText("R" + totalPrem + "0");
        }
    }//GEN-LAST:event_cmbBasicCoverPItemStateChanged

    private void btnSubmitBankDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitBankDetailsActionPerformed
        // TODO add your handling code here:
        updPaymentDetails();
    }//GEN-LAST:event_btnSubmitBankDetailsActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnClear2;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton btnExport;
    private javax.swing.JButton btnExtConfirm;
    private javax.swing.JButton btnNextBas;
    private javax.swing.JButton btnNextBas1;
    private javax.swing.JButton btnNextPay;
    private javax.swing.JButton btnPreviousBas;
    private javax.swing.JButton btnPreviousBas1;
    private javax.swing.JButton btnPreviousPay;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnSubmitBankDetails;
    private javax.swing.JButton btnSubmitBasPrem;
    private javax.swing.JButton btnSubmitExtPrem;
    private javax.swing.JCheckBox chkAgree;
    private javax.swing.JComboBox cmbAccountType;
    private javax.swing.JComboBox cmbBasicCover;
    private javax.swing.JComboBox cmbBasicCoverP;
    private javax.swing.JComboBox cmbCoverType;
    private javax.swing.JComboBox cmbExport;
    private javax.swing.JComboBox cmbPrint;
    private javax.swing.JComboBox cmbShowTable;
    private javax.swing.JComboBox cmbStatus;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JLabel lblClient;
    private javax.swing.JLabel lblClient1;
    private javax.swing.ButtonGroup planGroup;
    private javax.swing.JPanel pnlFinancials1;
    private javax.swing.JPanel pnlFinancials3;
    private javax.swing.JPanel pnlPolicyInfo;
    private javax.swing.JPanel pnlPremium;
    private javax.swing.JPanel pnlPremiumPayment;
    private javax.swing.ButtonGroup premiumGroup;
    private javax.swing.JRadioButton rbtnBnkDebitOrder;
    private javax.swing.JRadioButton rbtnCash;
    private javax.swing.JTabbedPane tabPremiums;
    private javax.swing.JTable tblPremiums;
    private javax.swing.JTextField txtAccountNo;
    private javax.swing.JTextField txtAgeOfExtIndi;
    private javax.swing.JTextField txtAgeOfIndi;
    private javax.swing.JTextField txtBasicPremium;
    private javax.swing.JTextField txtBranch;
    private javax.swing.JTextField txtBranchCode;
    private javax.swing.JTextField txtDayOfPayment;
    private javax.swing.JTextField txtExtCoverP;
    private javax.swing.JTextField txtExtPremAmountP;
    private javax.swing.JTextField txtNameOfBank;
    private javax.swing.JTextField txtPolcAge;
    private javax.swing.JTextField txtPolcAge1;
    private javax.swing.JTextField txtPolcBasCover;
    private javax.swing.JTextField txtPolcBasPremium;
    private javax.swing.JTextField txtPolcExtBasicCover;
    private javax.swing.JTextField txtPolcExtBasicPremium;
    private javax.swing.JTextField txtPolcExtStatus1;
    private javax.swing.JTextField txtPolcExtTotalPremium;
    private javax.swing.JTextField txtPolcGender;
    private javax.swing.JTextField txtPolcGender1;
    private javax.swing.JTextField txtPolcName;
    private javax.swing.JTextField txtPolcName1;
    private javax.swing.JTextField txtPolcStatus;
    private javax.swing.JTextField txtPolcSurname;
    private javax.swing.JTextField txtPolcSurname1;
    private javax.swing.JTextField txtPolcTtlPremium;
    private javax.swing.JTextField txtPolicyNo;
    private javax.swing.JTextField txtPolicyNo2;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtTotalPremium;
    // End of variables declaration//GEN-END:variables
}
