/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dariunhouse.g3.funeralscheme.form.policyoption;

import com.dariunhouse.g3.connection.ConnectServer;
import dbTables.Address;
import dbTables.Contact;
import dbTables.Family;
import dbTables.Person;
import java.awt.CardLayout;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author BT Rubber
 */
public class PolicyOption extends javax.swing.JPanel {

    ConnectServer connect = new ConnectServer();
    
    /**
     * Creates new form beneficiaries
     */
    public PolicyOption() {
        initComponents();
        
        getInfo2();
        
       /* lstSelectionMdl =  tblPolicyOption.getSelectionModel();
        lstSelectionMdl.addListSelectionListener(new SharedListSelectionHandler());
        tblPolicyOption.setSelectionModel(lstSelectionMdl);*/
        
//        CardLayout cl = (CardLayout)pnlFamilyDetails.getLayout();
//        cl.addLayoutComponent(pnl1p9Details1, "1");
//        cl.addLayoutComponent(pnl1p9Details2, "2");
//        
//        CardLayout cl2 = (CardLayout)pnlExtFamilyDetails.getLayout();
//        cl2.addLayoutComponent(pnlEfDetails1, "1");
//        cl2.addLayoutComponent(pnlEfDetails2, "2");
//        
//        CardLayout cl3 = (CardLayout)pnlFamilyDetails.getLayout();
//        cl3.addLayoutComponent(pnlFDetails1, "1");
////        cl3.addLayoutComponent(pnlFDetails2, "2");
    }
    
    
    

    private void getInfo(String polcId) {
        DefaultTableModel dtm = new DefaultTableModel();
        
        dtm.addColumn("Family ID", connect.famBeanLocal().getFamilyID(polcId).toArray());
        dtm.addColumn("Name", connect.famBeanLocal().getName(polcId).toArray());
        dtm.addColumn("Surname", connect.famBeanLocal().getSurname(polcId).toArray());
        dtm.addColumn("Relationship", connect.famBeanLocal().getRelationship(polcId).toArray());
        
        /*dtm.addColumn("Family ID", connect.famBeanLocal().getFamilyID(polcId).toArray());
        dtm.addColumn("Name", connect.famBeanLocal().getName(polcId).toArray());
        dtm.addColumn("Surname", connect.famBeanLocal().getSurname(polcId).toArray());
        dtm.addColumn("Relationship", connect.famBeanLocal().getRelationship(polcId).toArray());
        */
        
        /*System.out.println(connect.famBeanLocal().getFamilyID(polcId).toArray());
        System.out.println(connect.famBeanLocal().getName(polcId).toArray());
        */ 
         //tblFamily.setModel(dtm);
         //tblExtFamily.setModel(dtm);
         //tbl1Plus9Family.setModel(dtm);
        
        //tblPolicyOption.setModel(dtm);
    }
    
    private void getInfo2(){
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.addColumn("Client ID", connect.clientBeanLocal().getClientID().toArray());
        dtm.addColumn("ID Number", connect.clientBeanLocal().getClientIDNo().toArray());
        dtm.addColumn("Name", connect.clientBeanLocal().getClientName().toArray());
        dtm.addColumn("Surname", connect.clientBeanLocal().getClientSurname().toArray());
        dtm.addColumn("Policy No", connect.clientBeanLocal().getPolicyNo().toArray());
        dtm.addColumn("Cover Amount", connect.clientBeanLocal().getClientCoverAmount().toArray());
        dtm.addColumn("Premium", connect.clientBeanLocal().getMPremium().toArray());
        
        //tblPolicyOption.setModel(dtm);
    }
    
    private void getChildren(String name, String id){
        String[][] child = new String[5][1];
        child[0][0] = name;
    }
    
    String entityType = "";
    private void addExtensions(String entity){
        Person pers = new Person();
        Address addr = new Address();
        Contact cnct = new Contact();
        Family fam = new Family();
        
        if(entity.equals("Child")) {
        pers.setIdNumber(txtNationalID.getText());
        pers.setTitle(cmbTitle.getSelectedItem().toString());
        pers.setName(txtNames.getText());
        pers.setSurname(txtSurname.getText());
        pers.setGender(cmbGender.getSelectedItem().toString());
        pers.setAge(txtAge.getText());
        pers.setNationality(cmbNationality.getSelectedItem().toString());
        pers.setMaritalStatus(cmbMaritalStatus.getSelectedItem().toString());
        pers.setEconomicStatus(cmbEcoStatus.getSelectedItem().toString());
//        pers.setInitials(txtInitials.getText());
        pers.setRace(cmbRace.getSelectedItem().toString());
        
        cnct.setEntityId(txtNationalID.getText());
        cnct.setEntityType(entity);
        cnct.setCellNo1(txtCell.getText());
        cnct.setTelNo1(txtTelHome.getText());
        cnct.setTelNo2(txtTelWork.getText());
        cnct.setEmail(txtEmail.getText());
        cnct.setFax(txtFax.getText());
        
        addr.setUnitNo(txtUnitNo.getText());
        addr.setStreetName(txtStreetName.getText());
        addr.setSuburb(txtSuburb.getText());
        addr.setCityTown(txtCityTown.getText());
        addr.setProvince(cmbProvince.getSelectedItem().toString());
        addr.setCode(txtCode.getText());
        addr.setEntityId(txtNationalID.getText());
        addr.setEntityType(entity);
        
        fam.setPolicyNo(txtPolicyNoFam.getText());
        fam.setRelationship(entity);
        fam.setEntityId(txtNationalID.getText());
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        fam.setDateReg(sdf.format(dt));
        
        
        }
        else if(entity.equals("Extended Family")) {
            pers.setIdNumber(txtNationalID1.getText());
            pers.setTitle(cmbTitle1.getSelectedItem().toString());
            pers.setName(txtNames1.getText());
            pers.setSurname(txtSurname1.getText());
            pers.setGender(cmbGender1.getSelectedItem().toString());
            pers.setAge(txtAge1.getText());
            pers.setNationality(cmbNationality1.getSelectedItem().toString());
            pers.setMaritalStatus(cmbMaritalStatus1.getSelectedItem().toString());
            pers.setEconomicStatus(cmbEcoStatus1.getSelectedItem().toString());
//            pers.setInitials(txtNames1.getText().substring(0, 1) + ". ");
            pers.setRace(cmbRace1.getSelectedItem().toString());

            cnct.setEntityId(txtNationalID1.getText());
            cnct.setEntityType(entity);
            cnct.setCellNo1(txtCell1.getText());
            cnct.setCellNo1(txtCellNo21.getText());
            cnct.setTelNo1(txtTelHome1.getText());
            cnct.setTelNo2(txtTelWork1.getText());
            cnct.setEmail(txtEmail1.getText());
            cnct.setFax(txtFax1.getText());

            addr.setUnitNo(txtUnitNo1.getText());
            addr.setStreetName(txtStreetName1.getText());
            addr.setSuburb(txtSuburb1.getText());
            addr.setCityTown(txtCityTown1.getText());
            addr.setProvince(cmbProvince1.getSelectedItem().toString());
            addr.setCode(txtCode1.getText());
            addr.setEntityId(txtNationalID1.getText());
            addr.setEntityType(entity);

            fam.setPolicyNo(txtPolicyNoExtFam.getText());
            fam.setRelationship(cmbRelationship1.getSelectedItem().toString());
            fam.setFamilyType(entity);
            fam.setEntityId(txtNationalID1.getText());
            Date dt = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            fam.setDateReg(sdf.format(dt));

//            if(connect.famBeanLocal().addFamilyMember(pers, addr, cnct, fam).equals("success")){
//                System.out.println("Successful save.");
//                System.out.println(connect.famBeanLocal().updFam(pers));
//            }
        }
        else if(entity.equals("1Plus9")) {
            pers.setIdNumber(txtNationalID2.getText());
            pers.setTitle(cmbTitle2.getSelectedItem().toString());
            pers.setName(txtNames2.getText());
            pers.setSurname(txtSurname2.getText());
            pers.setGender(cmbGender2.getSelectedItem().toString());
            pers.setAge(txtAge2.getText());
            pers.setNationality(cmbNationality2.getSelectedItem().toString());
            pers.setMaritalStatus(cmbMaritalStatus2.getSelectedItem().toString());
            pers.setEconomicStatus(cmbEcoStatus2.getSelectedItem().toString());
//            pers.setInitials(txtInitials2.getText());
            pers.setRace(cmbRace2.getSelectedItem().toString());

            cnct.setEntityId(txtNationalID2.getText());
            cnct.setEntityType(entity);
            cnct.setCellNo1(txtCell2.getText());
            cnct.setCellNo1(txtCellNo22.getText());
            cnct.setTelNo1(txtTelHome2.getText());
            cnct.setTelNo2(txtTelWork2.getText());
            cnct.setEmail(txtEmail2.getText());
            cnct.setFax(txtFax2.getText());

            addr.setUnitNo(txtUnitNo2.getText());
            addr.setStreetName(txtStreetName2.getText());
            addr.setSuburb(txtSuburb2.getText());
            addr.setCityTown(txtCityTown2.getText());
            addr.setProvince(cmbProvince2.getSelectedItem().toString());
            addr.setCode(txtCode2.getText());
            addr.setEntityId(txtNationalID2.getText());
            addr.setEntityType(entity);

            fam.setPolicyNo(txtPolicyNo1Plus9.getText());
            fam.setRelationship(cmbRelationship2.getSelectedItem().toString());
            fam.setFamilyType(entity);
            fam.setEntityId(txtNationalID2.getText());
            Date dt = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            fam.setDateReg(sdf.format(dt));

//            if(connect.famBeanLocal().addFamilyMember(pers, addr, cnct, fam).equals("success")){
//                System.out.println("Successful save.");
//                System.out.println(connect.famBeanLocal().updFam(pers));
//            }
        }
        
        String res = "";
        if(tabPolicyOption.getSelectedIndex() == 0 && !txtPolicyNoFam.getText().isEmpty()){
            res = "ok";
        }
        else if(tabPolicyOption.getSelectedIndex() == 1 && !txtPolicyNoExtFam.getText().isEmpty()){
            res = "ok";
        }
        else if(tabPolicyOption.getSelectedIndex() == 2 && !txtPolicyNo1Plus9.getText().isEmpty()){
            res = "ok";
        }
        else {
            JOptionPane.showMessageDialog(null, "Please select a Policy Holder from the list below.");
        }
        if(res.equals("ok")){
            if(connect.famBeanLocal().addFamilyMember(pers, addr, cnct, fam).equals("success")) {
                
                
                if(tabPolicyOption.getSelectedIndex() == 0 && !txtPolicyNoFam.getText().isEmpty()){
                    
                    fam.setPolicyNo(txtPolicyNoFam.getText());
                    pers.setIdNumber(txtNationalID.getText());
                }
                else if(tabPolicyOption.getSelectedIndex() == 1 && !txtPolicyNoExtFam.getText().isEmpty()){
                    
                    fam.setPolicyNo(txtPolicyNoExtFam.getText());
                    pers.setIdNumber(txtNationalID1.getText());
                }
                else if(tabPolicyOption.getSelectedIndex() == 2 && !txtPolicyNo1Plus9.getText().isEmpty()){
                    
                    fam.setPolicyNo(txtPolicyNo1Plus9.getText());
                    pers.setIdNumber(txtNationalID2.getText());
                }
                
            if(connect.famBeanLocal().updIDs(fam, pers).equals("success")) {
                System.out.println("Successful save.");
            }
            
            JOptionPane.showMessageDialog(null, "Successful save.");
            }
        }
    }
    
    final TableRowSorter<TableModel> sorter1 = new TableRowSorter<TableModel>();
    ListSelectionModel lstSelectionMdl;
    
    /*class SharedListSelectionHandler implements ListSelectionListener {
    public void valueChanged(ListSelectionEvent e) {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        try{
           
            lstSelectionMdl.setSelectionMode(
                            ListSelectionModel.SINGLE_INTERVAL_SELECTION);

            if(tabPolicyOption.getSelectedIndex() == 0){
                txtPolicyNoFam.setText(tblPolicyOption.getValueAt(tblPolicyOption.getSelectedRow(), 4).toString());
                getInfo(txtPolicyNoFam.getText());
            }
            else if(tabPolicyOption.getSelectedIndex() == 1){
                txtPolicyNoExtFam.setText(tblPolicyOption.getValueAt(tblPolicyOption.getSelectedRow(), 4).toString());
                getInfo(txtPolicyNoExtFam.getText());
            }
            else if(tabPolicyOption.getSelectedIndex() == 2){
                txtPolicyNo1Plus9.setText(tblPolicyOption.getValueAt(tblPolicyOption.getSelectedRow(), 4).toString());
                getInfo(txtPolicyNo1Plus9.getText());
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    }*/
    
    private void searchByPolicyID(String policyID){
        //getDetails
        //Populate table
    }
    
    private void searchByHolderID(String holderID){
        //getDetails
        //Populate table
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        search_option = new javax.swing.JComboBox();
        search_field = new javax.swing.JTextField();
        search_button = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jButton4 = new javax.swing.JButton();
        jComboBox3 = new javax.swing.JComboBox();
        jButton5 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        tabPolicyOption = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        pnlChildrenInfo = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        cmbNationality = new javax.swing.JComboBox();
        jLabel123 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtAge = new javax.swing.JTextField();
        txtNames = new javax.swing.JTextField();
        txtSurname = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        cmbTitle = new javax.swing.JComboBox();
        jLabel28 = new javax.swing.JLabel();
        cmbGender = new javax.swing.JComboBox();
        jLabel21 = new javax.swing.JLabel();
        txtNationalID = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        jLabel77 = new javax.swing.JLabel();
        cmbEcoStatus = new javax.swing.JComboBox();
        cmbMaritalStatus = new javax.swing.JComboBox();
        jLabel41 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel124 = new javax.swing.JLabel();
        cmbRace = new javax.swing.JComboBox();
        jPanel12 = new javax.swing.JPanel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        txtTelWork = new javax.swing.JTextField();
        txtTelHome = new javax.swing.JTextField();
        txtCell = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txtFax = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        txtCellNo2 = new javax.swing.JTextField();
        jPanel20 = new javax.swing.JPanel();
        txtCode = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        txtUnitNo = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        txtStreetName = new javax.swing.JTextField();
        txtSuburb = new javax.swing.JTextField();
        txtCityTown = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        cmbProvince = new javax.swing.JComboBox();
        btnAddChild = new javax.swing.JButton();
        btnChildPrevious = new javax.swing.JButton();
        btnChildNext = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtPolicyNoFam = new javax.swing.JTextField();
        btnConfirm = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        pnlFamilyDetails = new javax.swing.JPanel();
        pnlFDetails1 = new javax.swing.JPanel();
        jLabel49 = new javax.swing.JLabel();
        jLabel76 = new javax.swing.JLabel();
        txtPolcName = new javax.swing.JTextField();
        txtPolcSurname = new javax.swing.JTextField();
        jLabel78 = new javax.swing.JLabel();
        txtPolcAge = new javax.swing.JTextField();
        jLabel79 = new javax.swing.JLabel();
        txtPolcGender = new javax.swing.JTextField();
        jLabel80 = new javax.swing.JLabel();
        txtPolcStatus = new javax.swing.JTextField();
        jLabel82 = new javax.swing.JLabel();
        txtPolcTotPremium = new javax.swing.JTextField();
        btnClear = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        pnlExtFamily = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jLabel84 = new javax.swing.JLabel();
        cmbEcoStatus1 = new javax.swing.JComboBox();
        cmbMaritalStatus1 = new javax.swing.JComboBox();
        cmbRace1 = new javax.swing.JComboBox();
        jLabel126 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jLabel81 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        cmbGender1 = new javax.swing.JComboBox();
        txtNationalID1 = new javax.swing.JTextField();
        txtNames1 = new javax.swing.JTextField();
        jLabel67 = new javax.swing.JLabel();
        txtSurname1 = new javax.swing.JTextField();
        cmbNationality1 = new javax.swing.JComboBox();
        jLabel68 = new javax.swing.JLabel();
        cmbTitle1 = new javax.swing.JComboBox();
        jLabel125 = new javax.swing.JLabel();
        txtAge1 = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        cmbRelationship1 = new javax.swing.JComboBox();
        jPanel18 = new javax.swing.JPanel();
        jLabel87 = new javax.swing.JLabel();
        jLabel88 = new javax.swing.JLabel();
        jLabel89 = new javax.swing.JLabel();
        txtTelWork1 = new javax.swing.JTextField();
        txtTelHome1 = new javax.swing.JTextField();
        txtCell1 = new javax.swing.JTextField();
        jLabel90 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        txtEmail1 = new javax.swing.JTextField();
        jLabel91 = new javax.swing.JLabel();
        txtFax1 = new javax.swing.JTextField();
        jLabel121 = new javax.swing.JLabel();
        txtCellNo21 = new javax.swing.JTextField();
        jPanel25 = new javax.swing.JPanel();
        txtCode1 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel92 = new javax.swing.JLabel();
        txtUnitNo1 = new javax.swing.JTextField();
        jLabel93 = new javax.swing.JLabel();
        jLabel94 = new javax.swing.JLabel();
        jLabel95 = new javax.swing.JLabel();
        jLabel96 = new javax.swing.JLabel();
        txtStreetName1 = new javax.swing.JTextField();
        txtSuburb1 = new javax.swing.JTextField();
        txtCityTown1 = new javax.swing.JTextField();
        jLabel97 = new javax.swing.JLabel();
        cmbProvince1 = new javax.swing.JComboBox();
        btnPrevious = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        btnAddExtFamily = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtPolicyNoExtFam = new javax.swing.JTextField();
        btnConfirm1 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        btnClear1 = new javax.swing.JButton();
        pnlExtFamilyDetails = new javax.swing.JPanel();
        pnlEfDetails2 = new javax.swing.JPanel();
        jLabel83 = new javax.swing.JLabel();
        txtPolcGender1 = new javax.swing.JTextField();
        jLabel85 = new javax.swing.JLabel();
        txtPolcStatus1 = new javax.swing.JTextField();
        jLabel86 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel98 = new javax.swing.JLabel();
        txtPolcTotPremium1 = new javax.swing.JTextField();
        txtPolcSurname1 = new javax.swing.JTextField();
        jLabel99 = new javax.swing.JLabel();
        txtPolcAge1 = new javax.swing.JTextField();
        txtPolcName1 = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        btnClear2 = new javax.swing.JButton();
        pnl1Plus9Details = new javax.swing.JPanel();
        pnl1p9Details2 = new javax.swing.JPanel();
        jLabel109 = new javax.swing.JLabel();
        txtPolcStatus2 = new javax.swing.JTextField();
        jLabel129 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel130 = new javax.swing.JLabel();
        txtPolcTotPremium2 = new javax.swing.JTextField();
        txtPolcName2 = new javax.swing.JTextField();
        txtPolcSurname2 = new javax.swing.JTextField();
        jLabel131 = new javax.swing.JLabel();
        jLabel133 = new javax.swing.JLabel();
        txtPolcAge2 = new javax.swing.JTextField();
        txtPolcGender2 = new javax.swing.JTextField();
        jPanel22 = new javax.swing.JPanel();
        pnl1Plus9 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jLabel104 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        txtAge2 = new javax.swing.JTextField();
        cmbGender2 = new javax.swing.JComboBox();
        txtNationalID2 = new javax.swing.JTextField();
        txtNames2 = new javax.swing.JTextField();
        jLabel74 = new javax.swing.JLabel();
        txtSurname2 = new javax.swing.JTextField();
        cmbNationality2 = new javax.swing.JComboBox();
        jLabel75 = new javax.swing.JLabel();
        jLabel128 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        cmbTitle2 = new javax.swing.JComboBox();
        jLabel56 = new javax.swing.JLabel();
        cmbRelationship2 = new javax.swing.JComboBox();
        jPanel27 = new javax.swing.JPanel();
        jLabel107 = new javax.swing.JLabel();
        cmbEcoStatus2 = new javax.swing.JComboBox();
        cmbMaritalStatus2 = new javax.swing.JComboBox();
        cmbRace2 = new javax.swing.JComboBox();
        jLabel127 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        jPanel28 = new javax.swing.JPanel();
        jLabel110 = new javax.swing.JLabel();
        jLabel111 = new javax.swing.JLabel();
        jLabel112 = new javax.swing.JLabel();
        txtTelWork2 = new javax.swing.JTextField();
        txtTelHome2 = new javax.swing.JTextField();
        txtCell2 = new javax.swing.JTextField();
        jLabel113 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        txtEmail2 = new javax.swing.JTextField();
        jLabel114 = new javax.swing.JLabel();
        txtFax2 = new javax.swing.JTextField();
        txtCellNo22 = new javax.swing.JTextField();
        jLabel122 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel29 = new javax.swing.JPanel();
        txtCode2 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel115 = new javax.swing.JLabel();
        txtUnitNo2 = new javax.swing.JTextField();
        jLabel116 = new javax.swing.JLabel();
        jLabel117 = new javax.swing.JLabel();
        jLabel118 = new javax.swing.JLabel();
        jLabel119 = new javax.swing.JLabel();
        txtStreetName2 = new javax.swing.JTextField();
        txtSuburb2 = new javax.swing.JTextField();
        txtCityTown2 = new javax.swing.JTextField();
        jLabel120 = new javax.swing.JLabel();
        cmbProvince2 = new javax.swing.JComboBox();
        btnPrevious1 = new javax.swing.JButton();
        btnNext1 = new javax.swing.JButton();
        btnAddExtFamily1 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        txtPolicyNo1Plus9 = new javax.swing.JTextField();
        btnConfirm2 = new javax.swing.JButton();

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Search:");

        search_option.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Policy Holder", "Policy ID" }));
        search_option.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_optionActionPerformed(evt);
            }
        });

        search_field.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_fieldActionPerformed(evt);
            }
        });

        search_button.setText("Search");
        search_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_buttonActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setText("Export To:");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Not Confirmed", "PDF", "Word (DocX)", "Excel (Xlsx)" }));

        jButton4.setText("Export");
        jButton4.setEnabled(false);

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Not Confirmed", "Form", "List" }));

        jButton5.setText("Print");
        jButton5.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(5, 5, 5)
                        .addComponent(search_option, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(search_field, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(search_button, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(search_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(search_button)
                    .addComponent(search_option, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 159, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5)))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        tabPolicyOption.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tabPolicyOptionStateChanged(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        pnlChildrenInfo.setLayout(new java.awt.CardLayout());

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Personal", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel34.setText("Page 1 Of 4");

        jLabel64.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel64.setText("Nationality");

        cmbNationality.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "South African Citizen", "Foreigner" }));
        cmbNationality.setMaximumSize(new java.awt.Dimension(233, 30));

        jLabel123.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel123.setText("Age");

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel22.setText("Title");

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel25.setText("Surname");

        txtAge.setColumns(2);
        txtAge.setMaximumSize(new java.awt.Dimension(233, 30));
        txtAge.setMinimumSize(null);
        txtAge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAgeActionPerformed(evt);
            }
        });
        txtAge.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtAgeKeyReleased(evt);
            }
        });

        txtNames.setColumns(2);
        txtNames.setMaximumSize(new java.awt.Dimension(233, 30));
        txtNames.setMinimumSize(null);
        txtNames.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNamesKeyReleased(evt);
            }
        });

        txtSurname.setColumns(2);
        txtSurname.setMaximumSize(new java.awt.Dimension(233, 30));
        txtSurname.setMinimumSize(null);
        txtSurname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSurnameKeyReleased(evt);
            }
        });

        jLabel65.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel65.setText("Names");

        cmbTitle.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Mr", "Miss", "Mrs" }));
        cmbTitle.setMaximumSize(new java.awt.Dimension(233, 30));
        cmbTitle.setMinimumSize(null);

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel28.setText("Gender");

        cmbGender.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Male", "Female" }));
        cmbGender.setMaximumSize(new java.awt.Dimension(233, 30));
        cmbGender.setMinimumSize(null);
        cmbGender.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbGenderItemStateChanged(evt);
            }
        });
        cmbGender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbGenderActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel21.setText("ID");

        txtNationalID.setColumns(2);
        txtNationalID.setMaximumSize(new java.awt.Dimension(233, 30));
        txtNationalID.setMinimumSize(null);
        txtNationalID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNationalIDActionPerformed(evt);
            }
        });
        txtNationalID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNationalIDKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel34))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel65)
                            .addComponent(jLabel64)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel28))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbTitle, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNames, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbGender, 0, 131, Short.MAX_VALUE)
                            .addComponent(cmbNationality, 0, 1, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel25)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel123))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtAge, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                            .addComponent(txtSurname, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNationalID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel64)
                    .addComponent(cmbNationality, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(txtNationalID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(txtSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel65)
                    .addComponent(txtNames, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(cmbTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel123)
                    .addComponent(txtAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(cmbGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(jLabel34)
                .addContainerGap())
        );

        pnlChildrenInfo.add(jPanel10, "card2");

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Economic", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel77.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel77.setText("Page 2 Of 4");

        cmbEcoStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Employed", "Unemployed", "Pensioner", "Students", "Grant Recipient" }));
        cmbEcoStatus.setMaximumSize(new java.awt.Dimension(233, 30));

        cmbMaritalStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Single", "Married", "Widow / Widower", "Divorced" }));
        cmbMaritalStatus.setMaximumSize(new java.awt.Dimension(233, 30));

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel41.setText("Marital Status");

        jLabel44.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel44.setText("Economics Status");

        jLabel124.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel124.setText("Race");

        cmbRace.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "African", "Asian", "Colored", "White" }));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel77))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel41)
                            .addComponent(jLabel44)
                            .addComponent(jLabel124))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbMaritalStatus, 0, 131, Short.MAX_VALUE)
                            .addComponent(cmbEcoStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbRace, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(157, 157, 157)))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel124)
                    .addComponent(cmbRace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel41)
                    .addComponent(cmbMaritalStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel44)
                    .addComponent(cmbEcoStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addComponent(jLabel77)
                .addContainerGap())
        );

        pnlChildrenInfo.add(jPanel11, "card5");

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Contact", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel36.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel36.setText("Tel (Work)");

        jLabel37.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel37.setText("Tel (Home)");

        jLabel38.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel38.setText("Cell");

        txtTelWork.setColumns(2);
        txtTelWork.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtTelHome.setColumns(2);
        txtTelHome.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtCell.setColumns(2);
        txtCell.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel39.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel39.setText("Page 3 Of 4");

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel26.setText("Email");

        txtEmail.setColumns(2);
        txtEmail.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel40.setText("Fax");

        txtFax.setColumns(2);
        txtFax.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel54.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel54.setText("Cell 2");

        txtCellNo2.setColumns(2);
        txtCellNo2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel39))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel54, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel37, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel38, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel36, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCell, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                            .addComponent(txtTelHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTelWork, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCellNo2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26)
                            .addComponent(jLabel40))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFax, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(txtTelWork, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(txtTelHome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40)
                    .addComponent(txtFax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(txtCell, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel54)
                    .addComponent(txtCellNo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel39)
                .addContainerGap())
        );

        pnlChildrenInfo.add(jPanel12, "card3");

        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Address", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        txtCode.setColumns(2);
        txtCode.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Code");

        jLabel42.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel42.setText("Province");

        txtUnitNo.setColumns(2);
        txtUnitNo.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel43.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel43.setText("Unit No");

        jLabel45.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel45.setText("City/Town");

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel46.setText("Suburb");

        jLabel47.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel47.setText("Street Name");

        txtStreetName.setColumns(2);
        txtStreetName.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtSuburb.setColumns(2);
        txtSuburb.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtCityTown.setColumns(2);
        txtCityTown.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel48.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel48.setText("Page 4 Of 4");

        cmbProvince.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "The Eastern Cape", "The Free State", "Gauteng", "KwaZulu-Natal", "Limpopo", "Mpumalanga", "The Northern Cape", "North West", "Western Cape" }));

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel20Layout.createSequentialGroup()
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel47)
                            .addComponent(jLabel45)
                            .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUnitNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtStreetName, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                            .addComponent(txtSuburb, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                            .addComponent(txtCityTown, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCode, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                            .addComponent(cmbProvince, 0, 1, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel48)))
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel43)
                    .addComponent(txtUnitNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42)
                    .addComponent(cmbProvince, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(txtStreetName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46)
                    .addComponent(txtSuburb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel45)
                    .addComponent(txtCityTown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(jLabel48)
                .addContainerGap())
        );

        pnlChildrenInfo.add(jPanel20, "card4");

        btnAddChild.setText("Add Child");
        btnAddChild.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddChildActionPerformed(evt);
            }
        });

        btnChildPrevious.setText("Previous");
        btnChildPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChildPreviousActionPerformed(evt);
            }
        });

        btnChildNext.setText("Next");
        btnChildNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChildNextActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Policy Number");

        txtPolicyNoFam.setColumns(2);

        btnConfirm.setText("Confirm");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlChildrenInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btnAddChild, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChildPrevious, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnChildNext, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPolicyNoFam)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtPolicyNoFam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirm))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlChildrenInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnChildPrevious)
                        .addComponent(btnChildNext))
                    .addComponent(btnAddChild))
                .addContainerGap())
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        pnlFamilyDetails.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Policy Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        pnlFamilyDetails.setLayout(new java.awt.CardLayout());

        jLabel49.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel49.setText("Name");

        jLabel76.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel76.setText("Surname");

        jLabel78.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel78.setText("Age");

        jLabel79.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel79.setText("Gender");

        jLabel80.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel80.setText("Status");

        jLabel82.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel82.setText("Total Premium");

        javax.swing.GroupLayout pnlFDetails1Layout = new javax.swing.GroupLayout(pnlFDetails1);
        pnlFDetails1.setLayout(pnlFDetails1Layout);
        pnlFDetails1Layout.setHorizontalGroup(
            pnlFDetails1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFDetails1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFDetails1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel49)
                    .addComponent(jLabel78)
                    .addComponent(jLabel80, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlFDetails1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtPolcAge, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(txtPolcName)
                    .addComponent(txtPolcStatus, javax.swing.GroupLayout.Alignment.LEADING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlFDetails1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel79)
                    .addComponent(jLabel76)
                    .addComponent(jLabel82))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlFDetails1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPolcGender)
                    .addComponent(txtPolcSurname)
                    .addComponent(txtPolcTotPremium, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlFDetails1Layout.setVerticalGroup(
            pnlFDetails1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFDetails1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFDetails1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel49)
                    .addComponent(txtPolcName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel76)
                    .addComponent(txtPolcSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlFDetails1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel78)
                    .addComponent(txtPolcAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel79)
                    .addComponent(txtPolcGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlFDetails1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel80)
                    .addComponent(txtPolcStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel82)
                    .addComponent(txtPolcTotPremium, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(103, Short.MAX_VALUE))
        );

        pnlFamilyDetails.add(pnlFDetails1, "card2");

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(pnlFamilyDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlFamilyDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        tabPolicyOption.addTab("Family", jPanel3);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        pnlExtFamily.setLayout(new java.awt.CardLayout());

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Economic", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel84.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel84.setText("Page 2 Of 4");

        cmbEcoStatus1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Employed", "Unemployed", "Pensioner", "Students", "Grant Recipient" }));
        cmbEcoStatus1.setMaximumSize(new java.awt.Dimension(233, 30));

        cmbMaritalStatus1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Single", "Married", "Widow / Widower", "Divorced" }));
        cmbMaritalStatus1.setMaximumSize(new java.awt.Dimension(233, 30));

        cmbRace1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "African", "Asian", "Colored", "White" }));

        jLabel126.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel126.setText("Race");

        jLabel69.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel69.setText("Marital Status");

        jLabel70.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel70.setText("Economics Status");

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel69)
                    .addComponent(jLabel70)
                    .addComponent(jLabel126))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cmbMaritalStatus1, javax.swing.GroupLayout.Alignment.LEADING, 0, 140, Short.MAX_VALUE)
                    .addComponent(cmbRace1, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbEcoStatus1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(141, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel84)
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel126)
                    .addComponent(cmbRace1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel69)
                    .addComponent(cmbMaritalStatus1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel70)
                    .addComponent(cmbEcoStatus1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addComponent(jLabel84)
                .addContainerGap())
        );

        pnlExtFamily.add(jPanel17, "card5");

        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Personal", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel81.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel81.setText("Page 1 Of 4");

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setText("ID");

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel24.setText("Title");

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel27.setText("Surname");

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel32.setText("Gender");

        cmbGender1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Male", "Female" }));
        cmbGender1.setMaximumSize(new java.awt.Dimension(233, 30));
        cmbGender1.setMinimumSize(null);
        cmbGender1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbGender1ItemStateChanged(evt);
            }
        });

        txtNationalID1.setColumns(2);
        txtNationalID1.setMaximumSize(new java.awt.Dimension(233, 30));
        txtNationalID1.setMinimumSize(null);
        txtNationalID1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNationalID1KeyReleased(evt);
            }
        });

        txtNames1.setColumns(2);
        txtNames1.setMaximumSize(new java.awt.Dimension(233, 30));
        txtNames1.setMinimumSize(null);
        txtNames1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNames1KeyReleased(evt);
            }
        });

        jLabel67.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel67.setText("Nationality");

        txtSurname1.setColumns(2);
        txtSurname1.setMaximumSize(new java.awt.Dimension(233, 30));
        txtSurname1.setMinimumSize(null);
        txtSurname1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSurname1KeyReleased(evt);
            }
        });

        cmbNationality1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "South African Citizen", "Foreigner" }));
        cmbNationality1.setMaximumSize(new java.awt.Dimension(233, 30));

        jLabel68.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel68.setText("Names");

        cmbTitle1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Mr", "Miss", "Mrs" }));
        cmbTitle1.setMaximumSize(new java.awt.Dimension(233, 30));
        cmbTitle1.setMinimumSize(null);

        jLabel125.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel125.setText("Age");

        txtAge1.setColumns(2);
        txtAge1.setMaximumSize(new java.awt.Dimension(233, 30));
        txtAge1.setMinimumSize(null);
        txtAge1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtAge1KeyReleased(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel55.setText("Relationship");

        cmbRelationship1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Grandparents", "Aunt", "Uncle", "Cousin", "Nephews", "Nieces", "Siblings-in-law" }));
        cmbRelationship1.setMaximumSize(new java.awt.Dimension(233, 30));
        cmbRelationship1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbRelationship1ItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel81))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel68)
                            .addComponent(jLabel67)
                            .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel32))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNames1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addComponent(cmbTitle1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(1, 1, 1))
                            .addComponent(cmbGender1, 0, 117, Short.MAX_VALUE)
                            .addComponent(cmbNationality1, 0, 1, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel27)
                            .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel125)
                            .addComponent(jLabel55))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSurname1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                            .addComponent(txtAge1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbRelationship1, javax.swing.GroupLayout.Alignment.TRAILING, 0, 1, Short.MAX_VALUE)
                            .addComponent(txtNationalID1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel67)
                    .addComponent(cmbNationality1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23)
                    .addComponent(txtNationalID1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel68)
                    .addComponent(txtNames1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27)
                    .addComponent(txtSurname1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(cmbTitle1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel125)
                    .addComponent(txtAge1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(cmbGender1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel55)
                    .addComponent(cmbRelationship1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(jLabel81)
                .addContainerGap())
        );

        pnlExtFamily.add(jPanel16, "card2");

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Contact", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel87.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel87.setText("Tel (Work)");

        jLabel88.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel88.setText("Tel (Home)");

        jLabel89.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel89.setText("Cell");

        txtTelWork1.setColumns(2);
        txtTelWork1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtTelHome1.setColumns(2);
        txtTelHome1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtCell1.setColumns(2);
        txtCell1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel90.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel90.setText("Page 3 Of 4");

        jLabel30.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel30.setText("Email");

        txtEmail1.setColumns(2);
        txtEmail1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel91.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel91.setText("Fax");

        txtFax1.setColumns(2);
        txtFax1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel121.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel121.setText("Cell 2");

        txtCellNo21.setColumns(2);
        txtCellNo21.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel90))
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel121, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel88, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel89, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel87, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCell1, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                            .addComponent(txtTelHome1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTelWork1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCellNo21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel30)
                            .addComponent(jLabel91))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFax1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtEmail1, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel87)
                    .addComponent(txtTelWork1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30)
                    .addComponent(txtEmail1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel88)
                    .addComponent(txtTelHome1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel91)
                    .addComponent(txtFax1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel89)
                    .addComponent(txtCell1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel121)
                    .addComponent(txtCellNo21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel90)
                .addContainerGap())
        );

        pnlExtFamily.add(jPanel18, "card3");

        jPanel25.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Address", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        txtCode1.setColumns(2);
        txtCode1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setText("Code");

        jLabel92.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel92.setText("Province");

        txtUnitNo1.setColumns(2);
        txtUnitNo1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel93.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel93.setText("Unit No");

        jLabel94.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel94.setText("City/Town");

        jLabel95.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel95.setText("Suburb");

        jLabel96.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel96.setText("Street Name");

        txtStreetName1.setColumns(2);
        txtStreetName1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtSuburb1.setColumns(2);
        txtSuburb1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtCityTown1.setColumns(2);
        txtCityTown1.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel97.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel97.setText("Page 4 Of 4");

        cmbProvince1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "The Eastern Cape", "The Free State", "Gauteng", "KwaZulu-Natal", "Limpopo", "Mpumalanga", "The Northern Cape", "North West", "Western Cape" }));

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel95, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel96)
                            .addComponent(jLabel94)
                            .addComponent(jLabel93, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUnitNo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtStreetName1, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                            .addComponent(txtSuburb1, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                            .addComponent(txtCityTown1, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel92, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCode1, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                            .addComponent(cmbProvince1, 0, 1, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel25Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel97)))
                .addContainerGap())
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel93)
                    .addComponent(txtUnitNo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel92)
                    .addComponent(cmbProvince1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel96)
                    .addComponent(txtStreetName1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(txtCode1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel95)
                    .addComponent(txtSuburb1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel94)
                    .addComponent(txtCityTown1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(jLabel97)
                .addContainerGap())
        );

        pnlExtFamily.add(jPanel25, "card4");

        btnPrevious.setText("Previous");
        btnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousActionPerformed(evt);
            }
        });

        btnNext.setText("Next");
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        btnAddExtFamily.setText("Add Family Member");
        btnAddExtFamily.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddExtFamilyActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Policy Number");

        txtPolicyNoExtFam.setColumns(2);

        btnConfirm1.setText("Confirm");
        btnConfirm1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirm1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlExtFamily, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(btnAddExtFamily)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPrevious, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNext, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPolicyNoExtFam)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConfirm1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtPolicyNoExtFam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirm1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlExtFamily, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrevious)
                    .addComponent(btnNext)
                    .addComponent(btnAddExtFamily))
                .addContainerGap())
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        btnClear1.setText("Clear");
        btnClear1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClear1ActionPerformed(evt);
            }
        });

        pnlExtFamilyDetails.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Policy Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        pnlExtFamilyDetails.setLayout(new java.awt.CardLayout());

        jLabel83.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel83.setText("Gender");

        jLabel85.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel85.setText("Status");

        jLabel86.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel86.setText("Total Premium");

        jLabel50.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel50.setText("Name");

        jLabel98.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel98.setText("Surname");

        jLabel99.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel99.setText("Age");

        javax.swing.GroupLayout pnlEfDetails2Layout = new javax.swing.GroupLayout(pnlEfDetails2);
        pnlEfDetails2.setLayout(pnlEfDetails2Layout);
        pnlEfDetails2Layout.setHorizontalGroup(
            pnlEfDetails2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEfDetails2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlEfDetails2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel50)
                    .addComponent(jLabel99)
                    .addComponent(jLabel85, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlEfDetails2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtPolcAge1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .addComponent(txtPolcName1)
                    .addComponent(txtPolcStatus1, javax.swing.GroupLayout.Alignment.LEADING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlEfDetails2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel83)
                    .addComponent(jLabel98)
                    .addComponent(jLabel86))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlEfDetails2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPolcGender1, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                    .addComponent(txtPolcSurname1)
                    .addComponent(txtPolcTotPremium1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        pnlEfDetails2Layout.setVerticalGroup(
            pnlEfDetails2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEfDetails2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlEfDetails2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel50)
                    .addComponent(jLabel98)
                    .addComponent(txtPolcSurname1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPolcName1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlEfDetails2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel99)
                    .addComponent(txtPolcAge1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel83)
                    .addComponent(txtPolcGender1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlEfDetails2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel85)
                    .addComponent(txtPolcStatus1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel86)
                    .addComponent(txtPolcTotPremium1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(103, Short.MAX_VALUE))
        );

        pnlExtFamilyDetails.add(pnlEfDetails2, "card3");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(btnClear1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlExtFamilyDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addComponent(pnlExtFamilyDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear1))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel15, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        tabPolicyOption.addTab("Extended Family", jPanel4);

        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        btnClear2.setText("Clear");
        btnClear2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClear2ActionPerformed(evt);
            }
        });

        pnl1Plus9Details.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Policy Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        pnl1Plus9Details.setLayout(new java.awt.CardLayout());

        jLabel109.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel109.setText("Status");

        jLabel129.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel129.setText("Total Premium");

        jLabel52.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel52.setText("Name");

        jLabel130.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel130.setText("Surname");

        jLabel131.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel131.setText("Age");

        jLabel133.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel133.setText("Gender");

        javax.swing.GroupLayout pnl1p9Details2Layout = new javax.swing.GroupLayout(pnl1p9Details2);
        pnl1p9Details2.setLayout(pnl1p9Details2Layout);
        pnl1p9Details2Layout.setHorizontalGroup(
            pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl1p9Details2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel52)
                    .addComponent(jLabel131)
                    .addComponent(jLabel109, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl1p9Details2Layout.createSequentialGroup()
                        .addComponent(txtPolcName2, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel133)
                            .addComponent(jLabel130))
                        .addGap(39, 39, 39))
                    .addGroup(pnl1p9Details2Layout.createSequentialGroup()
                        .addGroup(pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtPolcStatus2)
                            .addComponent(txtPolcAge2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel129)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPolcTotPremium2, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
                    .addComponent(txtPolcGender2, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
                    .addComponent(txtPolcSurname2))
                .addContainerGap())
        );
        pnl1p9Details2Layout.setVerticalGroup(
            pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl1p9Details2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel52)
                    .addComponent(jLabel130)
                    .addComponent(txtPolcSurname2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPolcName2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel131)
                    .addComponent(txtPolcAge2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel133)
                    .addComponent(txtPolcGender2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl1p9Details2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel109)
                    .addComponent(txtPolcStatus2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel129)
                    .addComponent(txtPolcTotPremium2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(103, Short.MAX_VALUE))
        );

        pnl1Plus9Details.add(pnl1p9Details2, "card3");

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(btnClear2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnl1Plus9Details, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                .addComponent(pnl1Plus9Details, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear2))
        );

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel21, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel22.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        pnl1Plus9.setLayout(new java.awt.CardLayout());

        jPanel23.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Personal", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel104.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel104.setText("Page 1 Of 4");

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel29.setText("ID");

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel33.setText("Surname");

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel35.setText("Gender");

        txtAge2.setColumns(2);
        txtAge2.setMaximumSize(new java.awt.Dimension(233, 30));
        txtAge2.setMinimumSize(null);
        txtAge2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtAge2KeyReleased(evt);
            }
        });

        cmbGender2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Male", "Female" }));
        cmbGender2.setMaximumSize(new java.awt.Dimension(233, 30));
        cmbGender2.setMinimumSize(null);
        cmbGender2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbGender2ItemStateChanged(evt);
            }
        });

        txtNationalID2.setColumns(2);
        txtNationalID2.setMaximumSize(new java.awt.Dimension(233, 30));
        txtNationalID2.setMinimumSize(null);
        txtNationalID2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNationalID2KeyReleased(evt);
            }
        });

        txtNames2.setColumns(2);
        txtNames2.setMaximumSize(new java.awt.Dimension(233, 30));
        txtNames2.setMinimumSize(null);
        txtNames2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNames2KeyReleased(evt);
            }
        });

        jLabel74.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel74.setText("Nationality");

        txtSurname2.setColumns(2);
        txtSurname2.setMaximumSize(new java.awt.Dimension(233, 30));
        txtSurname2.setMinimumSize(null);
        txtSurname2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSurname2KeyReleased(evt);
            }
        });

        cmbNationality2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "South African Citizen", "Foreigner" }));
        cmbNationality2.setMaximumSize(new java.awt.Dimension(233, 30));

        jLabel75.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel75.setText("Names");

        jLabel128.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel128.setText("Age");

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel31.setText("Title");

        cmbTitle2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Mr", "Miss", "Mrs" }));
        cmbTitle2.setMaximumSize(new java.awt.Dimension(233, 30));
        cmbTitle2.setMinimumSize(null);

        jLabel56.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel56.setText("Relationship");

        cmbRelationship2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Grandparents", "Aunt", "Uncle", "Cousin", "Nephews", "Nieces", "Siblings-in-law" }));
        cmbRelationship2.setMaximumSize(new java.awt.Dimension(233, 30));
        cmbRelationship2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbRelationship2ItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel104))
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel75)
                            .addComponent(jLabel74)
                            .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel35))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cmbGender2, 0, 114, Short.MAX_VALUE)
                            .addComponent(cmbTitle2, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNames2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbNationality2, javax.swing.GroupLayout.Alignment.LEADING, 0, 1, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel33)
                            .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel128)
                            .addComponent(jLabel56, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbRelationship2, 0, 113, Short.MAX_VALUE)
                            .addComponent(txtSurname2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtAge2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNationalID2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel74)
                    .addComponent(cmbNationality2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)
                    .addComponent(txtNationalID2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel75)
                    .addComponent(txtNames2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(txtSurname2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel128)
                    .addComponent(txtAge2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(cmbTitle2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(cmbGender2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel56)
                    .addComponent(cmbRelationship2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(jLabel104)
                .addContainerGap())
        );

        pnl1Plus9.add(jPanel23, "card2");

        jPanel27.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Economic", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel107.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel107.setText("Page 2 Of 4");

        cmbEcoStatus2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Employed", "Unemployed", "Pensioner", "Students", "Grant Recipient" }));
        cmbEcoStatus2.setMaximumSize(new java.awt.Dimension(233, 30));

        cmbMaritalStatus2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "Single", "Married", "Widow / Widower", "Divorced" }));
        cmbMaritalStatus2.setMaximumSize(new java.awt.Dimension(233, 30));

        cmbRace2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "African", "Asian", "Colored", "White" }));

        jLabel127.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel127.setText("Race");

        jLabel71.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel71.setText("Marital Status");

        jLabel72.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel72.setText("Economics Status");

        javax.swing.GroupLayout jPanel27Layout = new javax.swing.GroupLayout(jPanel27);
        jPanel27.setLayout(jPanel27Layout);
        jPanel27Layout.setHorizontalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel27Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel27Layout.createSequentialGroup()
                        .addGap(0, 316, Short.MAX_VALUE)
                        .addComponent(jLabel107))
                    .addGroup(jPanel27Layout.createSequentialGroup()
                        .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel71)
                            .addComponent(jLabel72)
                            .addComponent(jLabel127))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cmbMaritalStatus2, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbRace2, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbEcoStatus2, 0, 140, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel27Layout.setVerticalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel27Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel127)
                    .addComponent(cmbRace2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel71)
                    .addComponent(cmbMaritalStatus2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel72)
                    .addComponent(cmbEcoStatus2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addComponent(jLabel107)
                .addContainerGap())
        );

        pnl1Plus9.add(jPanel27, "card5");

        jPanel28.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Contact", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel110.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel110.setText("Tel (Work)");

        jLabel111.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel111.setText("Tel (Home)");

        jLabel112.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel112.setText("Cell");

        txtTelWork2.setColumns(2);
        txtTelWork2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtTelHome2.setColumns(2);
        txtTelHome2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtCell2.setColumns(2);
        txtCell2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel113.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel113.setText("Page 3 Of 4");

        jLabel53.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel53.setText("Email");

        txtEmail2.setColumns(2);
        txtEmail2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel114.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel114.setText("Fax");

        txtFax2.setColumns(2);
        txtFax2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtCellNo22.setColumns(2);
        txtCellNo22.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel122.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel122.setText("Cell 2");

        jButton1.setText("Use Polc Holder");

        javax.swing.GroupLayout jPanel28Layout = new javax.swing.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel28Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel113))
                    .addGroup(jPanel28Layout.createSequentialGroup()
                        .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel122, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel111, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel112, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel110, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCell2, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                            .addComponent(txtTelHome2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTelWork2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCellNo22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel28Layout.createSequentialGroup()
                                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel53)
                                    .addComponent(jLabel114))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtFax2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtEmail2, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)))
                            .addGroup(jPanel28Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton1)))))
                .addContainerGap())
        );
        jPanel28Layout.setVerticalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel110)
                    .addComponent(txtTelWork2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel53)
                    .addComponent(txtEmail2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel111)
                    .addComponent(txtTelHome2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel114)
                    .addComponent(txtFax2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel112)
                    .addComponent(txtCell2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel122)
                    .addComponent(txtCellNo22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel113)
                .addContainerGap())
        );

        pnl1Plus9.add(jPanel28, "card3");

        jPanel29.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Address", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        txtCode2.setColumns(2);
        txtCode2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("Code");

        jLabel115.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel115.setText("Province");

        txtUnitNo2.setColumns(2);
        txtUnitNo2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel116.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel116.setText("Unit No");

        jLabel117.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel117.setText("City/Town");

        jLabel118.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel118.setText("Suburb");

        jLabel119.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel119.setText("Street Name");

        txtStreetName2.setColumns(2);
        txtStreetName2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtSuburb2.setColumns(2);
        txtSuburb2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        txtCityTown2.setColumns(2);
        txtCityTown2.setMaximumSize(new java.awt.Dimension(233, 2147483647));

        jLabel120.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel120.setText("Page 4 Of 4");

        cmbProvince2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select", "The Eastern Cape", "The Free State", "Gauteng", "KwaZulu-Natal", "Limpopo", "Mpumalanga", "The Northern Cape", "North West", "Western Cape" }));

        javax.swing.GroupLayout jPanel29Layout = new javax.swing.GroupLayout(jPanel29);
        jPanel29.setLayout(jPanel29Layout);
        jPanel29Layout.setHorizontalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel29Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel29Layout.createSequentialGroup()
                        .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel118, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel119)
                            .addComponent(jLabel117)
                            .addComponent(jLabel116, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUnitNo2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtStreetName2, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                            .addComponent(txtSuburb2, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                            .addComponent(txtCityTown2, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel115, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCode2, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                            .addComponent(cmbProvince2, 0, 1, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel29Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel120)))
                .addContainerGap())
        );
        jPanel29Layout.setVerticalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel29Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel116)
                    .addComponent(txtUnitNo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel115)
                    .addComponent(cmbProvince2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel119)
                    .addComponent(txtStreetName2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(txtCode2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel118)
                    .addComponent(txtSuburb2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel117)
                    .addComponent(txtCityTown2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(jLabel120)
                .addContainerGap())
        );

        pnl1Plus9.add(jPanel29, "card4");

        btnPrevious1.setText("Previous");
        btnPrevious1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrevious1ActionPerformed(evt);
            }
        });

        btnNext1.setText("Next");
        btnNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNext1ActionPerformed(evt);
            }
        });

        btnAddExtFamily1.setText("Add 1 Of 9");
        btnAddExtFamily1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddExtFamily1ActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("Policy Number");

        txtPolicyNo1Plus9.setEditable(false);
        txtPolicyNo1Plus9.setColumns(2);

        btnConfirm2.setText("Confirm");
        btnConfirm2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirm2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnl1Plus9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel22Layout.createSequentialGroup()
                        .addComponent(btnAddExtFamily1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPrevious1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel22Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPolicyNo1Plus9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConfirm2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtPolicyNo1Plus9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirm2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl1Plus9, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrevious1)
                    .addComponent(btnNext1)
                    .addComponent(btnAddExtFamily1))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        tabPolicyOption.addTab("1 Plus 9", jPanel9);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabPolicyOption)
                .addGap(26, 26, 26))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPolicyOption)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void search_fieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_fieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_search_fieldActionPerformed

    private void btnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnlExtFamily.getLayout();
        cl.previous(pnlExtFamily);
    }//GEN-LAST:event_btnPreviousActionPerformed

    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnlExtFamily.getLayout();
        cl.next(pnlExtFamily);
    }//GEN-LAST:event_btnNextActionPerformed

    private void btnAddChildActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddChildActionPerformed
        // TODO add your handling code here:
        addExtensions("Child");
        
        getInfo2();
        getInfo(txtPolicyNoFam.getText());
        
        if(connect.famBeanLocal().getFamilyCount(txtPolicyNoFam.getText(), "Child") == 6) {
            JOptionPane.showMessageDialog(null, "You cannot add anymore records to your family list.");
            btnAddChild.setEnabled(false);
        }
    }//GEN-LAST:event_btnAddChildActionPerformed

    private void btnChildPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChildPreviousActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnlChildrenInfo.getLayout();
        cl.previous(pnlChildrenInfo);
    }//GEN-LAST:event_btnChildPreviousActionPerformed

    private void btnChildNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChildNextActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnlChildrenInfo.getLayout();
        cl.next(pnlChildrenInfo);
    }//GEN-LAST:event_btnChildNextActionPerformed

    private void btnAddExtFamilyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddExtFamilyActionPerformed
        // TODO add your handling code here:
        addExtensions("Extended Family");
        getInfo2();
        getInfo(txtPolicyNoExtFam.getText());
        
        if(connect.famBeanLocal().getExtFamilyCount(txtPolicyNoFam.getText(), "Extended Family") == 4) {
            JOptionPane.showMessageDialog(null, "You cannot add anymore records to your family list.");
            btnAddChild.setEnabled(false);
        }
    }//GEN-LAST:event_btnAddExtFamilyActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        txtPolicyNoFam.setEditable(true);
        btnAddChild.setEnabled(false);
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnPrevious1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrevious1ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnl1Plus9.getLayout();
        cl.previous(pnl1Plus9);
    }//GEN-LAST:event_btnPrevious1ActionPerformed

    private void btnNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNext1ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout)pnl1Plus9.getLayout();
        cl.next(pnl1Plus9);
    }//GEN-LAST:event_btnNext1ActionPerformed

    private void btnAddExtFamily1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddExtFamily1ActionPerformed
        // TODO add your handling code here:
        addExtensions("1Plus9");
        getInfo2();
        getInfo(txtPolicyNo1Plus9.getText());
        
        if(connect.famBeanLocal().get1Plus9Count(txtPolicyNoFam.getText(), "1Plus9") == 9) {
            JOptionPane.showMessageDialog(null, "You cannot add anymore records to your family list.");
            btnAddChild.setEnabled(false);
        }
    }//GEN-LAST:event_btnAddExtFamily1ActionPerformed

    private void btnClear1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClear1ActionPerformed
        // TODO add your handling code here:
        String polName = connect.famBeanLocal().getPersonId(txtPolicyNoExtFam.getText());
        JOptionPane.showMessageDialog(null, polName);
    }//GEN-LAST:event_btnClear1ActionPerformed

    private void tabPolicyOptionStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tabPolicyOptionStateChanged
        // TODO add your handling code here:
        //tblPolicyOption.clearSelection();
        
    }//GEN-LAST:event_tabPolicyOptionStateChanged

    private void txtNationalIDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNationalIDKeyReleased
        // TODO add your handling code here:
        //System.out.println(cmbNationality.getSelectedIndex()); 
        try{
            if(cmbNationality.getSelectedIndex() == 1){
                //System.out.println("Decision loop just executed");
                if(txtNationalID.getText().length() > 6){
                    int year = Integer.parseInt(txtNationalID.getText().substring(0, 2));
                    int month = Integer.parseInt(txtNationalID.getText().substring(2, 4));
                    int day = Integer.parseInt(txtNationalID.getText().substring(4, 6));
                    int gender = Integer.parseInt(txtNationalID.getText().substring(6, 7));
                    int age = 0;

                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                    
                    if(gender <5){
                        cmbGender.setSelectedItem("Female");
                        cmbTitle.setSelectedItem("Miss");
                        System.out.println("Female output");
                    } else {
                        cmbGender.setSelectedItem("Male");
                        cmbTitle.setSelectedItem("Mr");
                        System.out.println("Male output");
                    }

                    Date date = new Date();
                    age = date.getYear() - year;
                    if(month > date.getMonth()+1){
                        age--;
                    }
                    
                    txtAge.setText(age + "");
                }
            }
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.toString());
        }
        
        ///System.out.println("Key releasesed Does execute");
    }//GEN-LAST:event_txtNationalIDKeyReleased

    private void txtAgeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAgeKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAgeKeyReleased

    private void cmbGenderItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbGenderItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbGenderItemStateChanged

    private void txtNamesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNamesKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNamesKeyReleased

    private void txtSurnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSurnameKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSurnameKeyReleased

    private void txtAge1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAge1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAge1KeyReleased

    private void cmbGender1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbGender1ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbGender1ItemStateChanged

    private void txtNationalID1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNationalID1KeyReleased
        // TODO add your handling code here:
        
        
          try{
            if(cmbNationality1.getSelectedIndex() == 1){
                if(txtNationalID1.getText().length() > 6){
                    int year = Integer.parseInt(txtNationalID1.getText().substring(0, 2));
                    int month = Integer.parseInt(txtNationalID1.getText().substring(2, 4));
                    int day = Integer.parseInt(txtNationalID1.getText().substring(4, 6));
                    int gender = Integer.parseInt(txtNationalID1.getText().substring(6, 7));
                    int age = 0;

                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                     //jDateOfBirth.setDate(sdf.parse(month  + "/" + day + "/19" + year));
                    if(gender <5){
                        cmbGender1.setSelectedItem("Female");
                        cmbTitle1.setSelectedItem("Miss");
                    } else {
                        cmbGender1.setSelectedItem("Male");
                        cmbTitle1.setSelectedItem("Mr");
                    }
                    
                    Date date = new Date();
                    age = date.getYear() - year;
                    if(month > date.getMonth()+1){
                        age--;
                    }
                    txtAge1.setText(age + "");
                }
            }
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.toString());
        }
        
    }//GEN-LAST:event_txtNationalID1KeyReleased

    private void txtNames1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNames1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNames1KeyReleased

    private void txtSurname1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSurname1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSurname1KeyReleased

    private void txtAge2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAge2KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAge2KeyReleased

    private void cmbGender2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbGender2ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbGender2ItemStateChanged

    private void txtNationalID2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNationalID2KeyReleased
        // TODO add your handling code here:
              try{
            if(cmbNationality2.getSelectedIndex() == 1){
                if(txtNationalID2.getText().length() > 6){
                    int year = Integer.parseInt(txtNationalID2.getText().substring(0, 2));
                    int month = Integer.parseInt(txtNationalID2.getText().substring(2, 4));
                    int day = Integer.parseInt(txtNationalID2.getText().substring(4, 6));
                    int gender = Integer.parseInt(txtNationalID2.getText().substring(6, 7));
                    int age = 0;

                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                     //jDateOfBirth.setDate(sdf.parse(month  + "/" + day + "/19" + year));
                    if(gender <5){
                        cmbGender2.setSelectedItem("Female");
                        cmbTitle2.setSelectedItem("Miss");
                    } else {
                        cmbGender2.setSelectedItem("Male");
                        cmbTitle2.setSelectedItem("Mr");
                    }
                    
                    Date date = new Date();
                    age = date.getYear() - year;
                    if(month > date.getMonth()+1){
                        age--;
                    }
                    txtAge2.setText(age + "");
                }
            }
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.toString());
        }
        
    }//GEN-LAST:event_txtNationalID2KeyReleased

    private void txtNames2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNames2KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNames2KeyReleased

    private void txtSurname2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSurname2KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSurname2KeyReleased

    private void btnClear2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClear2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnClear2ActionPerformed

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        // TODO add your handling code here:
        txtPolcName.setText(connect.famBeanLocal().getSingleName(txtPolicyNoFam.getText()));
        txtPolcSurname.setText(connect.famBeanLocal().getSingleSurname(txtPolicyNoFam.getText()));
        txtPolcAge.setText(connect.famBeanLocal().getSingleAge(txtPolicyNoFam.getText()));
        txtPolcGender.setText(connect.famBeanLocal().getSingleGender(txtPolicyNoFam.getText()));
        txtPolcStatus.setText(connect.famBeanLocal().getSingleStatus(txtPolicyNoFam.getText()));
        txtPolcTotPremium.setText(connect.famBeanLocal().getSingleTotalPremium(txtPolicyNoFam.getText()));
        
        if(connect.famBeanLocal().getFamilyCount(txtPolicyNoFam.getText(), "Child") == 6) {
            JOptionPane.showMessageDialog(null, "You cannot add anymore records to your family list.");
            btnAddChild.setEnabled(false);
        }
    }//GEN-LAST:event_btnConfirmActionPerformed

    private void cmbRelationship1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbRelationship1ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbRelationship1ItemStateChanged

    private void cmbRelationship2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbRelationship2ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbRelationship2ItemStateChanged

    private void btnConfirm1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirm1ActionPerformed
        // TODO add your handling code here:
        txtPolcName1.setText(connect.famBeanLocal().getSingleName(txtPolicyNoExtFam.getText()));
        txtPolcSurname1.setText(connect.famBeanLocal().getSingleSurname(txtPolicyNoExtFam.getText()));
        txtPolcAge1.setText(connect.famBeanLocal().getSingleAge(txtPolicyNoExtFam.getText()));
        txtPolcGender1.setText(connect.famBeanLocal().getSingleGender(txtPolicyNoExtFam.getText()));
        txtPolcStatus1.setText(connect.famBeanLocal().getSingleStatus(txtPolicyNoExtFam.getText()));
        txtPolcTotPremium1.setText(connect.famBeanLocal().getSingleTotalPremium(txtPolicyNoExtFam.getText()));
        
        if(connect.famBeanLocal().getExtFamilyCount(txtPolicyNoFam.getText(), "Extended Family") == 4) {
            JOptionPane.showMessageDialog(null, "You cannot add anymore records to your family list.");
            btnAddChild.setEnabled(false);
        }
    }//GEN-LAST:event_btnConfirm1ActionPerformed

    private void btnConfirm2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirm2ActionPerformed
        // TODO add your handling code here:
        txtPolcName2.setText(connect.famBeanLocal().getSingleName(txtPolicyNo1Plus9.getText()));
        txtPolcSurname2.setText(connect.famBeanLocal().getSingleSurname(txtPolicyNo1Plus9.getText()));
        txtPolcAge2.setText(connect.famBeanLocal().getSingleAge(txtPolicyNo1Plus9.getText()));
        txtPolcGender2.setText(connect.famBeanLocal().getSingleGender(txtPolicyNo1Plus9.getText()));
        txtPolcStatus2.setText(connect.famBeanLocal().getSingleStatus(txtPolicyNo1Plus9.getText()));
        txtPolcTotPremium2.setText(connect.famBeanLocal().getSingleTotalPremium(txtPolicyNo1Plus9.getText()));
        
        if(connect.famBeanLocal().get1Plus9Count(txtPolicyNoFam.getText(), "1Plus9") == 4) {
            JOptionPane.showMessageDialog(null, "You cannot add anymore records to your family list.");
            btnAddChild.setEnabled(false);
        }
    }//GEN-LAST:event_btnConfirm2ActionPerformed

    private void cmbGenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbGenderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbGenderActionPerformed

    private void search_optionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_optionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_search_optionActionPerformed

    private void search_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_buttonActionPerformed
        // TODO add your handling code here:
        
        //String temp = search_option..getSelectedItem().toString();
        /*if(search_option.getSelectedItem().equals("Policy ID")){
            getInfo(search_field.getText());
        }
        else if(search_option.getSelectedItem().equals("Policy Holder")){
            System.out.println("Policy holder");
            searchByHolderID(search_field.getText());
        }*/
        //System.out.println(search_option.getSelectedItem().equals("Policy ID"));
        //System.out.println("search button executed");
    }//GEN-LAST:event_search_buttonActionPerformed

    private void txtNationalIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNationalIDActionPerformed
        // TODO add your handling code here:
        /*try{
            if(cmbNationality.getSelectedIndex() == 1){
                if(txtNationalID.getText().length() > 6){
                    int year = Integer.parseInt(txtNationalID.getText().substring(0, 2));
                    int month = Integer.parseInt(txtNationalID.getText().substring(2, 4));
                    int day = Integer.parseInt(txtNationalID.getText().substring(4, 6));
                    int gender = Integer.parseInt(txtNationalID.getText().substring(6, 7));
                    int age = 0;

                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    //                jDateOfBirth.setDate(sdf.parse(month  + "/" + day + "/19" + year));

                    if(gender <5){
                        cmbGender.setSelectedItem("Female");
                        cmbTitle.setSelectedItem("Miss");
                    } else {
                        cmbGender.setSelectedItem("Male");
                        cmbTitle.setSelectedItem("Mr");
                    }
                    
                    Date date = new Date();
                    age = date.getYear() - year;
                    if(month > date.getMonth()+1){
                        age--;
                    }
                    txtAge.setText(age + "");
                }
            }
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.toString());
        }*/
        
        /*System.out.println("Action has been performed");*/
    }//GEN-LAST:event_txtNationalIDActionPerformed

    private void txtAgeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAgeActionPerformed
         // TODO add your handling code here:
    }//GEN-LAST:event_txtAgeActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChild;
    private javax.swing.JButton btnAddExtFamily;
    private javax.swing.JButton btnAddExtFamily1;
    private javax.swing.JButton btnChildNext;
    private javax.swing.JButton btnChildPrevious;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnClear1;
    private javax.swing.JButton btnClear2;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton btnConfirm1;
    private javax.swing.JButton btnConfirm2;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnNext1;
    private javax.swing.JButton btnPrevious;
    private javax.swing.JButton btnPrevious1;
    private javax.swing.JComboBox cmbEcoStatus;
    private javax.swing.JComboBox cmbEcoStatus1;
    private javax.swing.JComboBox cmbEcoStatus2;
    private javax.swing.JComboBox cmbGender;
    private javax.swing.JComboBox cmbGender1;
    private javax.swing.JComboBox cmbGender2;
    private javax.swing.JComboBox cmbMaritalStatus;
    private javax.swing.JComboBox cmbMaritalStatus1;
    private javax.swing.JComboBox cmbMaritalStatus2;
    private javax.swing.JComboBox cmbNationality;
    private javax.swing.JComboBox cmbNationality1;
    private javax.swing.JComboBox cmbNationality2;
    private javax.swing.JComboBox cmbProvince;
    private javax.swing.JComboBox cmbProvince1;
    private javax.swing.JComboBox cmbProvince2;
    private javax.swing.JComboBox cmbRace;
    private javax.swing.JComboBox cmbRace1;
    private javax.swing.JComboBox cmbRace2;
    private javax.swing.JComboBox cmbRelationship1;
    private javax.swing.JComboBox cmbRelationship2;
    private javax.swing.JComboBox cmbTitle;
    private javax.swing.JComboBox cmbTitle1;
    private javax.swing.JComboBox cmbTitle2;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel107;
    private javax.swing.JLabel jLabel109;
    private javax.swing.JLabel jLabel110;
    private javax.swing.JLabel jLabel111;
    private javax.swing.JLabel jLabel112;
    private javax.swing.JLabel jLabel113;
    private javax.swing.JLabel jLabel114;
    private javax.swing.JLabel jLabel115;
    private javax.swing.JLabel jLabel116;
    private javax.swing.JLabel jLabel117;
    private javax.swing.JLabel jLabel118;
    private javax.swing.JLabel jLabel119;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel120;
    private javax.swing.JLabel jLabel121;
    private javax.swing.JLabel jLabel122;
    private javax.swing.JLabel jLabel123;
    private javax.swing.JLabel jLabel124;
    private javax.swing.JLabel jLabel125;
    private javax.swing.JLabel jLabel126;
    private javax.swing.JLabel jLabel127;
    private javax.swing.JLabel jLabel128;
    private javax.swing.JLabel jLabel129;
    private javax.swing.JLabel jLabel130;
    private javax.swing.JLabel jLabel131;
    private javax.swing.JLabel jLabel133;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JLabel jLabel97;
    private javax.swing.JLabel jLabel98;
    private javax.swing.JLabel jLabel99;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel pnl1Plus9;
    private javax.swing.JPanel pnl1Plus9Details;
    private javax.swing.JPanel pnl1p9Details2;
    private javax.swing.JPanel pnlChildrenInfo;
    private javax.swing.JPanel pnlEfDetails2;
    private javax.swing.JPanel pnlExtFamily;
    private javax.swing.JPanel pnlExtFamilyDetails;
    private javax.swing.JPanel pnlFDetails1;
    private javax.swing.JPanel pnlFamilyDetails;
    private javax.swing.JButton search_button;
    private javax.swing.JTextField search_field;
    private javax.swing.JComboBox search_option;
    private javax.swing.JTabbedPane tabPolicyOption;
    private javax.swing.JTextField txtAge;
    private javax.swing.JTextField txtAge1;
    private javax.swing.JTextField txtAge2;
    private javax.swing.JTextField txtCell;
    private javax.swing.JTextField txtCell1;
    private javax.swing.JTextField txtCell2;
    private javax.swing.JTextField txtCellNo2;
    private javax.swing.JTextField txtCellNo21;
    private javax.swing.JTextField txtCellNo22;
    private javax.swing.JTextField txtCityTown;
    private javax.swing.JTextField txtCityTown1;
    private javax.swing.JTextField txtCityTown2;
    private javax.swing.JTextField txtCode;
    private javax.swing.JTextField txtCode1;
    private javax.swing.JTextField txtCode2;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtEmail1;
    private javax.swing.JTextField txtEmail2;
    private javax.swing.JTextField txtFax;
    private javax.swing.JTextField txtFax1;
    private javax.swing.JTextField txtFax2;
    private javax.swing.JTextField txtNames;
    private javax.swing.JTextField txtNames1;
    private javax.swing.JTextField txtNames2;
    private javax.swing.JTextField txtNationalID;
    private javax.swing.JTextField txtNationalID1;
    private javax.swing.JTextField txtNationalID2;
    private javax.swing.JTextField txtPolcAge;
    private javax.swing.JTextField txtPolcAge1;
    private javax.swing.JTextField txtPolcAge2;
    private javax.swing.JTextField txtPolcGender;
    private javax.swing.JTextField txtPolcGender1;
    private javax.swing.JTextField txtPolcGender2;
    private javax.swing.JTextField txtPolcName;
    private javax.swing.JTextField txtPolcName1;
    private javax.swing.JTextField txtPolcName2;
    private javax.swing.JTextField txtPolcStatus;
    private javax.swing.JTextField txtPolcStatus1;
    private javax.swing.JTextField txtPolcStatus2;
    private javax.swing.JTextField txtPolcSurname;
    private javax.swing.JTextField txtPolcSurname1;
    private javax.swing.JTextField txtPolcSurname2;
    private javax.swing.JTextField txtPolcTotPremium;
    private javax.swing.JTextField txtPolcTotPremium1;
    private javax.swing.JTextField txtPolcTotPremium2;
    private javax.swing.JTextField txtPolicyNo1Plus9;
    private javax.swing.JTextField txtPolicyNoExtFam;
    private javax.swing.JTextField txtPolicyNoFam;
    private javax.swing.JTextField txtStreetName;
    private javax.swing.JTextField txtStreetName1;
    private javax.swing.JTextField txtStreetName2;
    private javax.swing.JTextField txtSuburb;
    private javax.swing.JTextField txtSuburb1;
    private javax.swing.JTextField txtSuburb2;
    private javax.swing.JTextField txtSurname;
    private javax.swing.JTextField txtSurname1;
    private javax.swing.JTextField txtSurname2;
    private javax.swing.JTextField txtTelHome;
    private javax.swing.JTextField txtTelHome1;
    private javax.swing.JTextField txtTelHome2;
    private javax.swing.JTextField txtTelWork;
    private javax.swing.JTextField txtTelWork1;
    private javax.swing.JTextField txtTelWork2;
    private javax.swing.JTextField txtUnitNo;
    private javax.swing.JTextField txtUnitNo1;
    private javax.swing.JTextField txtUnitNo2;
    // End of variables declaration//GEN-END:variables
}
