package com.dariunhouse.g3.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author BT Rubber
 */
public class Connect {
    public static Connection ConnectDb()
     {
        Connection conn = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String mysqlurl = "jdbc:mysql://localhost/g3funeralscheme";
            conn = DriverManager.getConnection(mysqlurl, "root", "root");
        }
        catch (ClassNotFoundException e)
        {
            //e.printStackTrace();
        }
        catch (SQLException e)
        {
            //e.printStackTrace();
        }
        return conn;
    }
}
