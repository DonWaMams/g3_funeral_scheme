/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dariunhouse.g3.connection;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import transactions.ClientSessionBeanRemote;
import transactions.FamilySessionBeanRemote;
import transactions.LoginSessionBeanRemote;
import transactions.PremiumsSessionBeanRemote;

/**
 *
 * @author Kapi1O
 */
public class ConnectServer {
    
    ClientSessionBeanRemote clientBean;
    FamilySessionBeanRemote famBean;
    LoginSessionBeanRemote loginBean;
    PremiumsSessionBeanRemote premiumBean;
    
    Properties props;
    private static InitialContext ctx;

    public void loadProperties(String h, String p) {
        try {
            props = new Properties();
 
            System.out.println("Host: " + h + " Port: " + p);
            
            props.setProperty("java.naming.factory.initial",
                    "com.sun.enterprise.naming.SerialInitContextFactory");
            props.setProperty("java.naming.factory.url.pkgs",
                    "com.sun.enterprise.naming");
            props.setProperty("java.naming.factory.state",
                    "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl"); 
            
            props.setProperty("org.omg.CORBA.ORBInitialHost", h);
            props.setProperty("org.omg.CORBA.ORBInitialPort", p);
            
            ctx = new InitialContext(props);
            
        } catch (NamingException ex) {
            Logger.getLogger(ConnectServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ClientSessionBeanRemote clientBeanLocal(){
        try{
            loadProperties("localhost", "3700");
            clientBean = (ClientSessionBeanRemote)ctx.lookup("transactions.ClientSessionBeanRemote");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        return clientBean;
    }
    
    public ClientSessionBeanRemote clientBeanRemote(){
        try{
            loadProperties("10.29.128.21", "3700");
            clientBean = (ClientSessionBeanRemote)ctx.lookup("java:global/G3_Funeral_Service-EJB/ClientSessionBean");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        return clientBean;
    }
    
    public FamilySessionBeanRemote famBeanLocal(){
        try{
            loadProperties("localhost", "3700");
            famBean = (FamilySessionBeanRemote)ctx.lookup("transactions.FamilySessionBeanRemote");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        return famBean;
    }
    
    public FamilySessionBeanRemote famBeanRemote(){
        try{
            loadProperties("10.29.128.21", "3700");
            famBean = (FamilySessionBeanRemote)ctx.lookup("java:global/G3_Funeral_Service-EJB/FamilySessionBean");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        return famBean;
    }
    
    public LoginSessionBeanRemote loginBeanLocal(){
        try{
            loadProperties("localhost", "3700");
            loginBean = (LoginSessionBeanRemote)ctx.lookup("transactions.LoginSessionBeanRemote");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        return loginBean;
    }
    
    public LoginSessionBeanRemote loginBeanRemote(){
        try{
            loadProperties("10.29.128.21", "3700");
            loginBean = (LoginSessionBeanRemote)ctx.lookup("java:global/G3_Funeral_Service-EJB/LoginSessionBean");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        return loginBean;
    }
    
    public PremiumsSessionBeanRemote premiumBeanLocal(){
        try{
            loadProperties("localhost", "3700");
            premiumBean = (PremiumsSessionBeanRemote)ctx.lookup("transactions.PremiumsSessionBeanRemote");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        return premiumBean;
    }
    
    public PremiumsSessionBeanRemote premiumBeanRemote(){
        try{
            loadProperties("10.29.128.21", "3700");
            premiumBean = (PremiumsSessionBeanRemote)ctx.lookup("java:global/G3_Funeral_Service-EJB/PremiumsSessionBean");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        return premiumBean;
    }
}
